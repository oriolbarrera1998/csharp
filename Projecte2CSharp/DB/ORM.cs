﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projecte2CSharp.DB
{
    public static class ORM
    {
        public static CEPLauncherEntities db = new CEPLauncherEntities();

        public static string MensajeError(SqlException sqlEx)
        {
            String mensaje = "";
            switch (sqlEx.Number)
            {
                case -1:
                    mensaje = "No s'ha pogut conectar amb el servidor";
                    break;
                case 547:
                    mensaje = "No es pot realitzar l'acció, té objectes relacionats";
                    break;
                case 2601:
                    mensaje ="Dades duplicades";
                    break;
                case 4060:
                    mensaje = "No es troba la base de dades";
                    break;
                case 18456:
                    mensaje = "Usuari o contrasenya incorrectes";
                    break;
                default:
                    mensaje = sqlEx.Number + " - " + sqlEx.Message;
                    break; 


            }

            return mensaje;
        }

        public static void rejectChanges()
        {
            foreach(DbEntityEntry entry in db.ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                    default: break;
                }
            }
        }

        public static string saveChanges()
        {
            string mensaje = "";

            try
            {
                ORM.db.SaveChanges();

            }catch(DbUpdateException ex)
            {
                ORM.rejectChanges();
                SqlException sqlEx = (SqlException)ex.InnerException.InnerException;
                mensaje = ORM.MensajeError(sqlEx);
            }

            return mensaje;     
        }
    }
}
