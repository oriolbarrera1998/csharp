﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projecte2CSharp.DB
{
    public static class ORMEsports
    {
        public static List<esports> selectEsports(ref string msg)
        {
            List<esports> _esports = null;

            try
            {
                _esports = (
                        from e in ORM.db.esports
                        orderby e.nom
                        select e).ToList();
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return _esports;
        }

        public static string insertEsport(esports esport)
        {
            ORM.db.esports.Add(esport);
            return ORM.saveChanges();
        }

        public static string updateEsport(esports esport)
        {
            
            esports e = ORM.db.esports.Find(esport.id);
            e = esport;
            return ORM.saveChanges();

        }

        public static string deleteEsport(esports esport)
        {
            ORM.db.esports.Remove(esport);
            return ORM.saveChanges();
        }
    }
}
