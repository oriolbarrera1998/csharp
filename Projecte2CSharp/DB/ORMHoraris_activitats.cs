﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projecte2CSharp.DB
{
    public static class ORMHoraris_activitats
    {
        public static List<horaris_activitats> selectHorarisActivitats(ref string msg)
        {
            List<horaris_activitats> _horaris = null;

            try
            {
                _horaris = (
                        from h in ORM.db.horaris_activitats
                        select h).ToList();
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return _horaris;
        }

        public static List<horaris_activitats> selectHorarisActivitatsByIdActivitat(int id, ref string msg)
        {
            List<horaris_activitats> _horaris = null;


            try
            {
                _horaris = (
                        from h in ORM.db.horaris_activitats
                        where h.id_activitat_concedida == id
                        select h).ToList();
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return _horaris;
        }

        public static string deleteHorariActivitat(horaris_activitats horari)
        {
            ORM.db.horaris_activitats.Remove(horari);
            return ORM.saveChanges();
        }

        public static horaris_activitats selectByHorariAdient(horaris_adients horari_adient, ref string msg, int id_activitatCondedida)
        {
            horaris_activitats horari_activitat = null;
           
          
            try
            {
                horari_activitat = (
                    from ha in ORM.db.horaris_activitats
                    where 
                    ha.id_dia_setmana == horari_adient.id_dia_setmana &&
                    horari_adient.id_activitat_demanada == id_activitatCondedida
                    select ha).FirstOrDefault();
            }catch(SqlException e){
                msg = ORM.MensajeError(e);
            }

            return horari_activitat;

        }
    }
}
