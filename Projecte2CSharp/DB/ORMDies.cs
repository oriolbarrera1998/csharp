﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projecte2CSharp.DB
{
    public static class ORMDies
    {

        //SELECT DIA PER ID
        public static dies_setmana getDiaByID(int id, ref string msg)
        {
            dies_setmana dia = null;
            try
            {
                dia = ORM.db.dies_setmana.Find(id);
            }catch(SqlException e)
            {
                msg = ORM.MensajeError(e); 
            }
            
            return dia;
        }


        //SELECT TOTS ELS DIES
        public static List<dies_setmana> getDies(ref string msg)
        {
            List<dies_setmana> _dies = null;

            try
            {
                _dies = (
                    from d in ORM.db.dies_setmana
                    select d).ToList();
            }catch(SqlException e)
            {
                msg = ORM.MensajeError(e);
            }
            return _dies;
        }
    }
}
