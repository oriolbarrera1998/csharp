﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.DB
{
    public static class ORMEntitats
    {


        //SELECT TOTES LES ENTITATS
        public static List<entitats> selectEntitats(ref string msg)
        {
            List<entitats> _entitats = null;
            try
            {
                string msg2 = "";
                int id_temporada = ((temporades)ORMTemporada.selectUltimaTemporada(ref msg2)).id;
                if (!msg2.Equals(""))
                {
                    MessageBox.Show(msg2, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    _entitats = (
                        from e in ORM.db.entitats
                        where e.borrat == false && e.id_temporada == id_temporada
                        orderby e.nom
                        select e).ToList();
                }
                
                
            }catch(SqlException ex)
            {
                msg = ORM.MensajeError(ex);
            }
            
            return _entitats;
        }

        public static entitats selectEntitatByID(int id, ref string msg)
        {
            entitats entitat = null;

            try
            {
                entitat = (from e in ORM.db.entitats
                           where e.id == id && e.borrat == false
                           select e).FirstOrDefault();
            }catch(SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return entitat;
        }

        
        //UPDATE ENTITAT
        public static string UpdateEntitat(entitats entitat)
        {
            string mensaje = "";
            entitats e = ORM.db.entitats.Find(entitat.id);
            e = entitat;
            mensaje = ORM.saveChanges();

            return mensaje;
        }


        //INSERT ENTITAT
        public static string insertEntitat(entitats entitat)
        {
            ORM.db.entitats.Add(entitat);
            return ORM.saveChanges();
        }


        //SOFT DELETE ENTITAT, ELS SEUS EQUIPS I TELEFONS
        public static string deleteEntitat(entitats entitat)
        {
            //ORM.db.entitats.Remove(entitat);
            entitat.borrat = true;
            foreach(equips equip in entitat.equips)
            {
                string msg = ORMEquips.deleteEquip(equip);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            foreach(telefons t in entitat.telefons)
            {
                ORMTelefons.deleteTelefon(t);
            }
            entitats e = ORM.db.entitats.Find(entitat.id);
            e = entitat;

            return ORM.saveChanges();
        }


        //SELECT ENTITATS PER CIF
        public static List<entitats> selectEntitatByCIF(string cif, ref string msg)
        {
            List<entitats> _entitats = null;

            try
            {
                string msg2 = "";
                int id_temporada = ((temporades)ORMTemporada.selectUltimaTemporada(ref msg2)).id;
                if (!msg2.Equals(""))
                {
                    MessageBox.Show(msg2, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    _entitats = (
                        from e in ORM.db.entitats
                        where e.cif.Contains(cif) && e.id_temporada == id_temporada
                        orderby e.nom
                        select e).ToList();
                }
                            
            }
            catch (SqlException ex)
            {
                msg = ORM.MensajeError(ex);
            }
            return _entitats;
        }


        //SELECT ENTITATS PER NOM
        public static List<entitats> selectEntitatByNom(string nom, ref string msg)
        {
            List<entitats> _entitats = null;

            try
            {
                string msg2 = "";
                int id_temporada = ((temporades)ORMTemporada.selectUltimaTemporada(ref msg2)).id;
                if (!msg2.Equals(""))
                {
                    MessageBox.Show(msg2, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    _entitats = (
                        from e in ORM.db.entitats
                        where e.nom.Contains(nom) && e.id_temporada == id_temporada
                        orderby e.nom
                        select e).ToList();
                } 
                
            }
            catch (SqlException ex)
            {
                msg = ORM.MensajeError(ex);
            }

            return _entitats;
        }



        //GENERA A PARTIR DE UNA ENTITAT, LA COPIA D'EQUIPS, TELEFONS I LA PROPIA ENTITAT AMB LA TEMPORADA ACTUALITZADA
        public static string generarTemporada(temporades temporada)
        {
            string msg = "";
            msg = ORMTemporada.insertTemporada(temporada);
            foreach (entitats e in ORM.db.entitats.ToList())
            {
                if (!e.borrat)
                {
                    int id = e.id;
                    entitats entitat = new entitats();
                    entitat.borrat = e.borrat;
                    entitat.cif = e.cif;
                    entitat.correu = e.correu;
                    entitat.direccio = e.direccio;
                    entitat.facebook = e.facebook;
                    entitat.instagram = e.instagram;
                    entitat.latitud = e.latitud;
                    entitat.longitud = e.longitud;
                    entitat.nom = e.nom;
                    entitat.id_temporada = ORMTemporada.selectUltimaTemporada(ref msg).id;
                    entitat.password = e.password;
                    entitat.twitter = e.twitter;


                    ORMEntitats.insertEntitat(entitat);
                   
                    
                    ORM.saveChanges();

                    entitats entitatNova = (
                        from ent in ORM.db.entitats
                        orderby ent.id descending
                        select ent
                        ).FirstOrDefault();

                    foreach (equips equip in e.equips.ToList())
                    {
                        if (!equip.borrat)
                        {
                            equips eq = new equips();
                            eq.id_categoria = equip.id_categoria;
                            eq.id_categoria_competicio = equip.id_categoria_competicio;
                            eq.id_competicio = equip.id_competicio;
                            eq.id_entitat = entitatNova.id;
                            eq.id_esport = equip.id_esport;
                            eq.id_sexe = equip.id_sexe;
                            eq.nom = equip.nom;

                            msg = ORMEquips.insertEquip(eq);
                            
                            //ORM.saveChanges();
                        }


                    }

                    foreach (telefons t in e.telefons)
                    {
                        if (!t.borrat)
                        {
                            telefons tel = new telefons();
                            tel.id_entitat = entitatNova.id;
                            tel.rao = t.rao;
                            tel.telefon = t.telefon;

                            ORMTelefons.insertTelefon(tel);
                        }

                    }
                }
                
            }
            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return ORM.saveChanges();
            
        }
    }

    
}
