﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.DB
{
    public static class ORMEquips
    {

        //SELECT EQUIPS PER ID DE LA ENTITAT
        public static List<equips> selectEquipsByEntitat(int id_entitat, ref string msg)
        {
            List<equips> _equips = null;

            try
            {
                string msg2 = "";
                int id_temporada = ((temporades)ORMTemporada.selectUltimaTemporada(ref msg2)).id;
                if (!msg2.Equals(""))
                {
                    MessageBox.Show(msg2, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    _equips = (
                        from e in ORM.db.equips
                        where (e.id_entitat == id_entitat && e.borrat == false && e.entitats.id_temporada == id_temporada)
                        orderby e.nom
                        select e).ToList();
                }
                
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return _equips;
        }


        //SELECT TOTS ELS EQUIPS
        public static List<equips> selectEquips(ref string msg)
        {
            List<equips> _equips = null;


            try
            {
                string msg2 = "";
                int id_temporada = ((temporades)ORMTemporada.selectUltimaTemporada(ref msg2)).id;
                if (!msg2.Equals(""))
                {
                    MessageBox.Show(msg2, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    _equips = (
                        from e in ORM.db.equips
                        where e.borrat == false && e.entitats.id_temporada == id_temporada
                        orderby e.nom
                        select e).ToList();
                }
   
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return _equips;
        }



        //SELECT EQUIPS PER NOM I ENTITAT
        public static List<equips> selectEquipsByNomIEntitat(int id_entitat, string nom, ref string msg)
        {

            List<equips> _equips = null;


            try
            {
                string msg2 = "";
                int id_temporada = ((temporades)ORMTemporada.selectUltimaTemporada(ref msg2)).id;
                if (!msg2.Equals(""))
                {
                    MessageBox.Show(msg2, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    _equips = (
                       from e in ORM.db.equips
                       where (e.id_entitat == id_entitat && e.nom.Contains(nom) && e.borrat == false && e.entitats.id_temporada == id_temporada)
                       orderby e.nom
                       select e).ToList();
                }
                
                
            }
            catch (SqlException e)
            {

                msg = ORM.MensajeError(e);
            }

            return _equips;
        }


        //SELECT EQUIPS PER NOM
        public static List<equips> selectEquipsByNom(string nom, ref string msg)
        {
            List<equips> _equips = null;

            try
            {
                string msg2 = "";
                int id_temporada = ((temporades)ORMTemporada.selectUltimaTemporada(ref msg2)).id;
                if (!msg2.Equals(""))
                {
                    MessageBox.Show(msg2, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    _equips = (
                        from e in ORM.db.equips
                        where e.nom.Contains(nom) && e.borrat == false && e.entitats.id_temporada == id_temporada
                        orderby e.nom
                        select e).ToList();
                }
  
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return _equips;
        }


        //INSERT EQUIP
        public static string insertEquip(equips equip)
        {
            equip.borrat = false;
            ORM.db.equips.Add(equip);
            return ORM.saveChanges();
        }


        //UPDATE EQUIP
        public static string updateEquip(equips equip)
        {
            equips e = ORM.db.equips.Find(equip.id);
            e = equip;
            return ORM.saveChanges();
        }


        //SOFT-DELETE EQUIP
        public static string deleteEquip(equips equip)
        {
            equip.borrat = true;
            equips eq = ORM.db.equips.Find(equip.id);
            eq = equip;
            return ORM.saveChanges();
        }


        //SELECT EQUIP BY ID
        public static equips selectEquipByID(int id, ref string msg)
        {
            equips equip = ORM.db.equips.Find(id);
            return equip;
        }

    }
}
