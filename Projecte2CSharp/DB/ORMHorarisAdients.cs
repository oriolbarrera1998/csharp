﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projecte2CSharp.DB
{
    public static class ORMHorarisAdients
    {
        public static List<horaris_adients> selectHorarisAdients(int id_demanda, ref string msg)
        {
            List<horaris_adients> _horaris = null;

            try
            {
                _horaris = (from h in ORM.db.horaris_adients
                            where h.id_activitat_demanada == id_demanda
                            select h).ToList();

                foreach (horaris_adients h in _horaris)
                {
                    h.assignatString = (bool)h.assignat ? "Si" : "No";
                    
                }
            }
            catch(SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            

            return _horaris;
        }

        public static string updateHorariAdient(horaris_adients h)
        {
            horaris_adients horari = ORM.db.horaris_adients.Find(h.id);
            horari = h;

            return ORM.saveChanges();
        }

    }
}
