﻿using Projecte2CSharp.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.Forms
{
    public partial class FormTelefon : Form
        
    {
        public Boolean formatOkay;
        public Boolean isModification { get; set; }
        public telefons telefon { get; set; }
        public int id_entitat { get; set; }

        private entitats entitat;

        public FormTelefon(entitats entitat)
        {
            InitializeComponent();
            this.entitat = entitat;
        }

        public FormTelefon(telefons tel)
        {
            InitializeComponent();
            this.telefon = tel;
            
            this.isModification = true;
        }

        private void FormTelefon_Load(object sender, EventArgs e)
        {
            if (isModification)
            {
                textBoxRao.Text = telefon.rao;
                textBoxTelefon.Text = telefon.telefon;
                this.Text = "Modificar telefon";
            }
            else
            {
                telefon = new telefons();
                this.Text = "Modificar telefon";
            }
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSaveChanges_Click(object sender, EventArgs e)
        {
            if (isModification)
            {
                telefon.rao = textBoxRao.Text;
                telefon.telefon = textBoxTelefon.Text;
                ORMTelefons.updateTelefon(telefon);
            }
            else
            {
                telefon.borrat = false;
                telefon.rao = textBoxRao.Text;
                telefon.telefon = textBoxTelefon.Text;
                entitat.telefons.Add(telefon);
            }
            
            this.Close();
        }

        private void textBoxTelefon_TextChanged(object sender, EventArgs e)
        {
            String pattern = @"^(\d{9})$";

            Regex rx = new Regex(pattern);

            Match match = rx.Match(textBoxTelefon.Text);

            if (match.Success)
            {
                textBoxTelefon.BackColor = ColorTranslator.FromHtml("#7ec97c");
            }
            else
            {
                textBoxTelefon.BackColor = ColorTranslator.FromHtml("#ea8585");
            }
        }
    }
}
