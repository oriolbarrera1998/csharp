﻿using Projecte2CSharp.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.Forms
{
    public partial class NewEsportForm : Form
    {
        public esports esport { get; set; }
        public bool isModification { get; set; }

        public NewEsportForm()
        {
            InitializeComponent();
            
        }

        public NewEsportForm(esports esport)
        {
            isModification = true;
            this.esport = esport;
            InitializeComponent();
        }

        private void buttonSaveChanges_Click(object sender, EventArgs e)
        {
            if (!isModification) { esport = new esports(); }

            esport.nom = textBoxNom.Text;
            string msg = "";
            if (isModification)
            {
                ORMEsports.insertEsport(esport);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                ORMEsports.insertEsport(esport);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            this.Close();
            
            
            
            this.Close();
        }

        private void NewEsportForm_Load(object sender, EventArgs e)
        {
            if (isModification)
            {
                textBoxNom.Text = esport.nom;
                this.Text = esport.nom;
            }
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
