﻿using Projecte2CSharp.Clases;
using Projecte2CSharp.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.Forms
{
    public partial class EntitatForm : Form
    {
        public Boolean inputOkay;
        public entitats entitat { get; set; }
        public Boolean isModification { get; set; }

        public EntitatForm()
        {
            InitializeComponent();
        }

        public EntitatForm(entitats entitat)
        {
            this.entitat = entitat;
            this.isModification = true;
            InitializeComponent();
        }


        private void EntitatForm_Load(object sender, EventArgs e)
        {
            
            if (isModification)
            {
                string msg = "";
                telefonsBindingSource.DataSource = ORMTelefons.getTelefonsByEntitat(entitat.id).ToList();
                bindingSourceEquips.DataSource = ORMEquips.selectEquipsByEntitat(entitat.id, ref msg).ToList();
                bindingSourceEsports.DataSource = ORMEsports.selectEsports(ref msg);
                bindingSourceSexes.DataSource = ORMSexes.selectSexes(ref msg);
                bindingSourceCategories.DataSource = ORMCategories.selectCategories(ref msg);
                
                bindingSourceCompeticio.DataSource = ORMCompeticio.selectCompeticions(ref msg);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


                this.Text = "Entitat: " + entitat.nom;
               
                textBoxNom.Text = entitat.nom;
                textBoxCIF.Text = entitat.cif;
                textBoxDireccio.Text = entitat.direccio;
                textBoxCorreu.Text = entitat.correu;

                labelNomEntitat.Text = entitat.nom;
                buttonGeneratPassword.Enabled = false;
                textBoxPassword.Enabled = false;
            }
            else
            {
                entitat = new entitats();
                this.Text = "Nova entitat";
                labelNomEntitat.Text = "Nova entitat";
            }
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSaveChanges_Click(object sender, EventArgs e)
        {
            saveChanges();
            this.Close();
        }


        private void saveChanges()
        {
            if (isModification)
            {
                entitat.nom = textBoxNom.Text;
                entitat.cif = textBoxCIF.Text;
                entitat.direccio = textBoxDireccio.Text;
                entitat.correu = textBoxCorreu.Text;
                string mensaje = ORMEntitats.UpdateEntitat(entitat);
                if (!mensaje.Equals(""))
                {
                    MessageBox.Show(mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                string msg = "";
                entitat.nom = textBoxNom.Text;
                entitat.cif = textBoxCIF.Text;
                entitat.direccio = textBoxDireccio.Text;
                entitat.correu = textBoxCorreu.Text;
                entitat.id_temporada = ORMTemporada.selectUltimaTemporada(ref msg).id;

                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                entitat.password = Hash.hashString(textBoxPassword.Text);

                msg = ORMEntitats.insertEntitat(entitat);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void buttonGeneratPassword_Click(object sender, EventArgs e)
        {
            textBoxPassword.Text = generarPassword();
        }

        

        private void textBoxNom_TextChanged(object sender, EventArgs e)
        {
            labelNomEntitat.Text = textBoxNom.Text;
        }

       
        private void buttonEditTelefon_Click(object sender, EventArgs e)
        {
            if(dataGridViewTelefons.RowCount != 0)
            {
                FormTelefon ft = new FormTelefon((telefons)dataGridViewTelefons.CurrentRow.DataBoundItem);
                ft.ShowDialog();
            }
            
        }

        private void buttonAddTelefon_Click(object sender, EventArgs e)
        {
            FormTelefon ft = new FormTelefon(entitat);
            ft.ShowDialog();
        }

        private void dataGridViewTelefons_DoubleClick(object sender, EventArgs e)
        {
            FormTelefon ft = new FormTelefon((telefons)dataGridViewTelefons.CurrentRow.DataBoundItem);
            ft.ShowDialog();
        }

        private void EntitatForm_Activated(object sender, EventArgs e)
        {
            if (entitat != null)
            {
                if (isModification)
                {
                    saveChanges();
                }
                
                string msg = "";
                telefonsBindingSource.DataSource = ORMTelefons.getTelefonsByEntitat(entitat.id).ToList();
                bindingSourceEquips.DataSource = ORMEquips.selectEquipsByEntitat(entitat.id, ref msg).ToList();

                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {

            }
            
        }

        private void dataGridViewTelefons_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            telefons tel = (telefons)dataGridViewTelefons.CurrentRow.DataBoundItem;
            DialogResult result = MessageBox.Show("Segur que vols eliminar el telefon: " + tel.rao + "?", "Eliminar telefon", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if(result == DialogResult.Yes)
            {
                ORMTelefons.deleteTelefon(tel);

            }
            else
            {
                e.Cancel = true;
            }
        }

        private void buttonEliminarTelefon_Click(object sender, EventArgs e)
        {
            telefons tel = (telefons)dataGridViewTelefons.CurrentRow.DataBoundItem;
            DialogResult result = MessageBox.Show("Segur que vols eliminar el telefon: " + tel.rao + "?", "Eliminar telefon", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                ORMTelefons.deleteTelefon(tel);
                telefonsBindingSource.DataSource = ORMTelefons.getTelefonsByEntitat(entitat.id).ToList();
            }
        }


        #region Funcions personalitzades

        public string generarPassword()
        {
            StringBuilder sb = new StringBuilder();
            Random random = new Random();

            char caracter;

            for (int i = 0; i < 10; i++)
            {
                caracter = (char)random.Next(33, 122);
                sb.Append(caracter);
            }


            return sb.ToString();
        }

        private void buttonGeneratPassword_Click_1(object sender, EventArgs e)
        {
            textBoxPassword.Text = generarPassword();
        }


        #endregion

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (isModification)
            {
                string msg = "";
                bindingSourceEquips.DataSource = ORMEquips.selectEquipsByNomIEntitat(entitat.id, textBoxBuscar.Text, ref msg);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
        }

        private void buttonAddEquip_Click(object sender, EventArgs e)
        {
            EquipForm ef = new EquipForm(entitat);
            ef.ShowDialog();
        }

        

        private void dataGridViewEquips_DoubleClick_1(object sender, EventArgs e)
        {
            EquipForm equipform = new EquipForm((equips)dataGridViewEquips.CurrentRow.DataBoundItem);
            equipform.ShowDialog();
        }

        private void buttonEliminarEquip_Click(object sender, EventArgs e)
        {
            equips equip = (equips)dataGridViewEquips.CurrentRow.DataBoundItem;
            DialogResult result = MessageBox.Show("Segur que vols eliminar el equip: " + equip.nom + "?", "Eliminar equip", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                string msg = ORMEquips.deleteEquip(equip);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
               
            }
        }

        private void buttonEditEquip_Click(object sender, EventArgs e)
        {
            if(dataGridViewEquips.RowCount != 0)
            {
                EquipForm ef = new EquipForm((equips)dataGridViewEquips.CurrentRow.DataBoundItem);
                ef.ShowDialog();
            }
            
            
        }

        private void dataGridViewEquips_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            equips eq = (equips)dataGridViewEquips.CurrentRow.DataBoundItem;
            DialogResult result = MessageBox.Show("Segur que vols eliminar el equip: " + eq.nom + "?", "Eliminar equip", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                string msg = ORMEquips.deleteEquip(eq);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                //bindingSourceEquips.DataSource = ORMEquips.selectEquipsByEntitat(entitat.id).ToList();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void textBoxCIF_TextChanged(object sender, EventArgs e)
        {
            String pattern = @"^([A-Z]\d{8})$";

            Regex rx = new Regex(pattern);

            Match match = rx.Match(textBoxCIF.Text);

            if(match.Success)
            {
                textBoxCIF.BackColor = ColorTranslator.FromHtml("#FFFFFF");
            }
            else
            {
                textBoxCIF.BackColor = ColorTranslator.FromHtml("#EA8585");
            }
        }

        private void textBoxCorreu_TextChanged(object sender, EventArgs e)
        {
            String pattern = @"^(.*)@\w+(\.\w+)+$";

            Regex rx = new Regex(pattern);

            Match match = rx.Match(textBoxCorreu.Text);

            if (match.Success)
            {
                textBoxCorreu.BackColor = ColorTranslator.FromHtml("#FFFFFF");
            }
            else
            {
                textBoxCorreu.BackColor = ColorTranslator.FromHtml("#ea8585");
            }
        }
    }
}
