﻿using Projecte2CSharp.Clases;
using Projecte2CSharp.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.Forms
{
    public partial class NewUsuariForm : Form
    {
        public usuaris usuari { get; set; }
        public bool isModification { get; set; }

        public NewUsuariForm()
        {
            InitializeComponent();
        }

        public NewUsuariForm(usuaris usuari)
        {
            this.usuari = usuari;
            isModification = true;
            InitializeComponent();
        }

        private void NewUsuariForm_Load(object sender, EventArgs e)
        {
            if (isModification)
            {
                textBoxPassword.Enabled = false;
                labelAdmin.Text = usuari.nom;
                buttonGeneratPassword.Enabled = false;
                checkBox1.Checked = (bool)usuari.superadmin ? true : false;
                textBoxNom.Text = usuari.nom;
                textBoxCorreu.Text = usuari.correu;
                textBoxUsername.Text = usuari.nomUsuari;
                this.Text = usuari.nom;
            }
            else
            {
                this.Text = "Nou usuari";
            }
        }

        private void buttonGeneratPassword_Click(object sender, EventArgs e)
        {
            textBoxPassword.Text = generarPassword();
        }

        public string generarPassword()
        {
            StringBuilder sb = new StringBuilder();
            Random random = new Random();

            char caracter;

            for (int i = 0; i < 10; i++)
            {
                caracter = (char)random.Next(33, 122);
                sb.Append(caracter);
            }


            return sb.ToString();
        }

        private void buttonSaveChanges_Click(object sender, EventArgs e)
        {
            string msg = "";
            if (!isModification) { usuari = new usuaris(); }

            usuari.nom = textBoxNom.Text;
            usuari.correu = textBoxCorreu.Text;
            usuari.nomUsuari = textBoxUsername.Text;
            usuari.superadmin = checkBox1.Checked ? true : false;

            if (isModification)
            {
               msg = ORMUsuaris.updateUsuari(usuari);
            }
            else
            {
                usuari.contrasenya = Hash.hashString(textBoxPassword.Text);
                msg = ORMUsuaris.insertUsuari(usuari);
            }

            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBoxNom_TextChanged(object sender, EventArgs e)
        {
            labelAdmin.Text = textBoxNom.Text;
        }

        private void textBoxCorreu_TextChanged(object sender, EventArgs e)
        {
            String pattern = @"^(.*)@\w+(\.\w+)+$";

            Regex rx = new Regex(pattern);

            Match match = rx.Match(textBoxCorreu.Text);

            if (match.Success)
            {
                textBoxCorreu.BackColor = ColorTranslator.FromHtml("#FFFFFF");
            }
            else
            {
                textBoxCorreu.BackColor = ColorTranslator.FromHtml("#ea8585");
            }
        }
    }
}
