﻿using Projecte2CSharp.Clases.POJO;
using Projecte2CSharp.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.Forms
{
    public partial class EntitatDemandesForm : Form
    {
        public entitats entitat { get; set; }
        public EntitatDemandesForm()
        {
            InitializeComponent();
        }

        public EntitatDemandesForm(entitats entitat)
        {
            this.entitat = entitat;
            InitializeComponent();
        }

        private void EntitatDemandesForm_Load(object sender, EventArgs e)
        {
            this.Text = "Demandes: " + entitat.nom;
            label1.Text = "Demandes: " + entitat.nom;

            getData();
        }

        public void getData()
        {
            string msg = "";
            bindingSourceEquips.DataSource = ORMEquips.selectEquipsByEntitat(entitat.id, ref msg);
            bindingSourceEsport.DataSource = ORMEsports.selectEsports(ref msg);
            bindingSourceCategories.DataSource = ORMCategories.selectCategories(ref msg);
            bindingSourceCompeticions.DataSource = ORMCompeticio.selectCompeticions(ref msg);
            bindingSourceSexes.DataSource = ORMSexes.selectSexes(ref msg);

            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridViewEquips_DoubleClick(object sender, EventArgs e)
        {
            string msg = "";
            equips equip = (equips)dataGridViewEquips.CurrentRow.DataBoundItem;
            List<ActivitatDemanada> act = ActivitatDemanada.createDemandes(ORMActivitatsConcedides.getActivitatsDemanadesByEquip(equip.id, ref msg));
            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                
                ActivitatDemanada activitat = null;
                List<ActivitatDemanada> act_ent = new List<ActivitatDemanada>();
                foreach (ActivitatDemanada a in act)
                {
                    if (a.entitat == entitat.nom)
                    {
                        act_ent.Add(a);
                    }
                }
                FormDemanda fd;
                if (act_ent.Count > 0)
                {
                    foreach (ActivitatDemanada actD in act_ent)
                    {
                        fd = new FormDemanda(actD, MainForm.user.id);
                        fd.Show();
                    }

                }
                else
                {
                    MessageBox.Show("L'equip seleccionat no te cap demanda", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void dataGridViewEquips_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
