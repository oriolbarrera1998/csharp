﻿namespace Projecte2CSharp.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panelMenu = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.buttonConfig = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.sidePanel = new System.Windows.Forms.Panel();
            this.buttonEquips = new System.Windows.Forms.Button();
            this.buttonDemandes = new System.Windows.Forms.Button();
            this.buttonInstalacions = new System.Windows.Forms.Button();
            this.buttonEntitats = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.entitatsControls1 = new Projecte2CSharp.UserControls.EntitatsControls();
            this.instalacionsControl1 = new Projecte2CSharp.UserControls.InstalacionsControl();
            this.equipsControl = new Projecte2CSharp.UserControls.EquipsControl();
            this.configControl = new Projecte2CSharp.UserControls.ConfigControl();
            this.demandesControl1 = new Projecte2CSharp.UserControls.DemandesControl();
            this.panelMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(89)))), ((int)(((byte)(91)))));
            this.panelMenu.Controls.Add(this.pictureBox2);
            this.panelMenu.Controls.Add(this.buttonConfig);
            this.panelMenu.Controls.Add(this.button4);
            this.panelMenu.Controls.Add(this.pictureBox1);
            this.panelMenu.Controls.Add(this.sidePanel);
            this.panelMenu.Controls.Add(this.buttonEquips);
            this.panelMenu.Controls.Add(this.buttonDemandes);
            this.panelMenu.Controls.Add(this.buttonInstalacions);
            this.panelMenu.Controls.Add(this.buttonEntitats);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(252, 730);
            this.panelMenu.TabIndex = 0;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = global::Projecte2CSharp.Properties.Resources.information;
            this.pictureBox2.Location = new System.Drawing.Point(12, 688);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(28, 30);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // buttonConfig
            // 
            this.buttonConfig.FlatAppearance.BorderSize = 0;
            this.buttonConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonConfig.Font = new System.Drawing.Font("Calibri Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConfig.ForeColor = System.Drawing.Color.White;
            this.buttonConfig.Image = global::Projecte2CSharp.Properties.Resources.settings_gears;
            this.buttonConfig.Location = new System.Drawing.Point(20, 543);
            this.buttonConfig.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonConfig.Name = "buttonConfig";
            this.buttonConfig.Size = new System.Drawing.Size(296, 90);
            this.buttonConfig.TabIndex = 7;
            this.buttonConfig.Text = "   Configuració";
            this.buttonConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonConfig.UseVisualStyleBackColor = true;
            this.buttonConfig.Click += new System.EventHandler(this.buttonConfig_Click);
            // 
            // button4
            // 
            this.button4.BackgroundImage = global::Projecte2CSharp.Properties.Resources.logout;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(205, 688);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(30, 30);
            this.button4.TabIndex = 6;
            this.toolTip1.SetToolTip(this.button4, "Tancar sessió\r\n");
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Projecte2CSharp.Properties.Resources.logo_principal_blanc;
            this.pictureBox1.Location = new System.Drawing.Point(25, 57);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(194, 72);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // sidePanel
            // 
            this.sidePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.sidePanel.Location = new System.Drawing.Point(0, 183);
            this.sidePanel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.sidePanel.Name = "sidePanel";
            this.sidePanel.Size = new System.Drawing.Size(20, 90);
            this.sidePanel.TabIndex = 4;
            // 
            // buttonEquips
            // 
            this.buttonEquips.FlatAppearance.BorderSize = 0;
            this.buttonEquips.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEquips.Font = new System.Drawing.Font("Calibri Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEquips.ForeColor = System.Drawing.Color.White;
            this.buttonEquips.Image = global::Projecte2CSharp.Properties.Resources.support;
            this.buttonEquips.Location = new System.Drawing.Point(20, 453);
            this.buttonEquips.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonEquips.Name = "buttonEquips";
            this.buttonEquips.Size = new System.Drawing.Size(260, 90);
            this.buttonEquips.TabIndex = 3;
            this.buttonEquips.Text = "       Equips";
            this.buttonEquips.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonEquips.UseVisualStyleBackColor = true;
            this.buttonEquips.Click += new System.EventHandler(this.buttonEquips_Click_1);
            // 
            // buttonDemandes
            // 
            this.buttonDemandes.FlatAppearance.BorderSize = 0;
            this.buttonDemandes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDemandes.Font = new System.Drawing.Font("Calibri Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDemandes.ForeColor = System.Drawing.Color.White;
            this.buttonDemandes.Image = global::Projecte2CSharp.Properties.Resources.notes;
            this.buttonDemandes.Location = new System.Drawing.Point(20, 363);
            this.buttonDemandes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonDemandes.Name = "buttonDemandes";
            this.buttonDemandes.Size = new System.Drawing.Size(284, 90);
            this.buttonDemandes.TabIndex = 2;
            this.buttonDemandes.Text = "    Demandes";
            this.buttonDemandes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonDemandes.UseVisualStyleBackColor = true;
            this.buttonDemandes.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonInstalacions
            // 
            this.buttonInstalacions.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonInstalacions.FlatAppearance.BorderSize = 0;
            this.buttonInstalacions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInstalacions.Font = new System.Drawing.Font("Calibri Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonInstalacions.ForeColor = System.Drawing.Color.White;
            this.buttonInstalacions.Image = global::Projecte2CSharp.Properties.Resources.soccer_field_top_view_stroke_symbol__2_;
            this.buttonInstalacions.Location = new System.Drawing.Point(20, 273);
            this.buttonInstalacions.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonInstalacions.Name = "buttonInstalacions";
            this.buttonInstalacions.Size = new System.Drawing.Size(296, 90);
            this.buttonInstalacions.TabIndex = 1;
            this.buttonInstalacions.Text = "    Instal·lacions";
            this.buttonInstalacions.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonInstalacions.UseVisualStyleBackColor = true;
            this.buttonInstalacions.Click += new System.EventHandler(this.buttonInstalacions_Click);
            // 
            // buttonEntitats
            // 
            this.buttonEntitats.FlatAppearance.BorderSize = 0;
            this.buttonEntitats.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEntitats.Font = new System.Drawing.Font("Calibri Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEntitats.ForeColor = System.Drawing.Color.White;
            this.buttonEntitats.Image = global::Projecte2CSharp.Properties.Resources.multiple_users_silhouette;
            this.buttonEntitats.Location = new System.Drawing.Point(20, 183);
            this.buttonEntitats.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonEntitats.Name = "buttonEntitats";
            this.buttonEntitats.Size = new System.Drawing.Size(260, 90);
            this.buttonEntitats.TabIndex = 0;
            this.buttonEntitats.Text = "       Entitats";
            this.buttonEntitats.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonEntitats.UseVisualStyleBackColor = true;
            this.buttonEntitats.Click += new System.EventHandler(this.buttonEntitats_Click);
            // 
            // entitatsControls1
            // 
            this.entitatsControls1.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.entitatsControls1.Location = new System.Drawing.Point(251, 0);
            this.entitatsControls1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.entitatsControls1.Name = "entitatsControls1";
            this.entitatsControls1.Size = new System.Drawing.Size(979, 730);
            this.entitatsControls1.TabIndex = 1;
            // 
            // instalacionsControl1
            // 
            this.instalacionsControl1.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.instalacionsControl1.Location = new System.Drawing.Point(251, 0);
            this.instalacionsControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.instalacionsControl1.Name = "instalacionsControl1";
            this.instalacionsControl1.Size = new System.Drawing.Size(979, 730);
            this.instalacionsControl1.TabIndex = 2;
            // 
            // equipsControl
            // 
            this.equipsControl.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipsControl.Location = new System.Drawing.Point(251, 0);
            this.equipsControl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.equipsControl.Name = "equipsControl";
            this.equipsControl.Size = new System.Drawing.Size(985, 730);
            this.equipsControl.TabIndex = 3;
            // 
            // configControl
            // 
            this.configControl.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configControl.Location = new System.Drawing.Point(251, 0);
            this.configControl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.configControl.Name = "configControl";
            this.configControl.Size = new System.Drawing.Size(985, 730);
            this.configControl.TabIndex = 4;
            this.configControl.user = null;
            // 
            // demandesControl1
            // 
            this.demandesControl1.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.demandesControl1.Location = new System.Drawing.Point(251, 0);
            this.demandesControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.demandesControl1.Name = "demandesControl1";
            this.demandesControl1.Size = new System.Drawing.Size(985, 730);
            this.demandesControl1.TabIndex = 5;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1233, 730);
            this.Controls.Add(this.demandesControl1);
            this.Controls.Add(this.configControl);
            this.Controls.Add(this.equipsControl);
            this.Controls.Add(this.instalacionsControl1);
            this.Controls.Add(this.entitatsControls1);
            this.Controls.Add(this.panelMenu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajuntament de Sant Cugat";
            this.Activated += new System.EventHandler(this.MainForm_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panelMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Button buttonEntitats;
        private System.Windows.Forms.Button buttonEquips;
        private System.Windows.Forms.Button buttonDemandes;
        private System.Windows.Forms.Button buttonInstalacions;
        private System.Windows.Forms.Panel sidePanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ToolTip toolTip1;
        private UserControls.EntitatsControls entitatsControls1;
        private UserControls.InstalacionsControl instalacionsControl1;
        private System.Windows.Forms.Button buttonConfig;
        private UserControls.EquipsControl equipsControl;
        private UserControls.ConfigControl configControl;
        private UserControls.DemandesControl demandesControl1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}