﻿namespace Projecte2CSharp.Forms
{
    partial class EntitatDemandesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EntitatDemandesForm));
            this.dataGridViewEquips = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idcategoriaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bindingSourceCategories = new System.Windows.Forms.BindingSource(this.components);
            this.idesportDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bindingSourceEsport = new System.Windows.Forms.BindingSource(this.components);
            this.idsexeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bindingSourceSexes = new System.Windows.Forms.BindingSource(this.components);
            this.idcompeticioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bindingSourceCompeticions = new System.Windows.Forms.BindingSource(this.components);
            this.idcategoriacompeticioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.borratDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.bindingSourceEquips = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEquips)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCategories)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEsport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceSexes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCompeticions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEquips)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewEquips
            // 
            this.dataGridViewEquips.AllowUserToAddRows = false;
            this.dataGridViewEquips.AllowUserToDeleteRows = false;
            this.dataGridViewEquips.AllowUserToResizeColumns = false;
            this.dataGridViewEquips.AllowUserToResizeRows = false;
            this.dataGridViewEquips.AutoGenerateColumns = false;
            this.dataGridViewEquips.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewEquips.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewEquips.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridViewEquips.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewEquips.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewEquips.ColumnHeadersHeight = 35;
            this.dataGridViewEquips.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewEquips.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nomDataGridViewTextBoxColumn,
            this.idcategoriaDataGridViewTextBoxColumn,
            this.idesportDataGridViewTextBoxColumn,
            this.idsexeDataGridViewTextBoxColumn,
            this.idcompeticioDataGridViewTextBoxColumn,
            this.idcategoriacompeticioDataGridViewTextBoxColumn,
            this.borratDataGridViewCheckBoxColumn});
            this.dataGridViewEquips.DataSource = this.bindingSourceEquips;
            this.dataGridViewEquips.EnableHeadersVisualStyles = false;
            this.dataGridViewEquips.Location = new System.Drawing.Point(27, 95);
            this.dataGridViewEquips.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dataGridViewEquips.MultiSelect = false;
            this.dataGridViewEquips.Name = "dataGridViewEquips";
            this.dataGridViewEquips.ReadOnly = true;
            this.dataGridViewEquips.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewEquips.RowHeadersVisible = false;
            this.dataGridViewEquips.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewEquips.RowTemplate.Height = 35;
            this.dataGridViewEquips.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewEquips.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewEquips.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewEquips.Size = new System.Drawing.Size(1021, 515);
            this.dataGridViewEquips.TabIndex = 24;
            this.dataGridViewEquips.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewEquips_CellContentClick);
            this.dataGridViewEquips.DoubleClick += new System.EventHandler(this.dataGridViewEquips_DoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Source Sans Pro", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 31);
            this.label1.TabIndex = 25;
            this.label1.Text = "label1";
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // nomDataGridViewTextBoxColumn
            // 
            this.nomDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nomDataGridViewTextBoxColumn.DataPropertyName = "nom";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.nomDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.nomDataGridViewTextBoxColumn.HeaderText = "Nom";
            this.nomDataGridViewTextBoxColumn.Name = "nomDataGridViewTextBoxColumn";
            this.nomDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // idcategoriaDataGridViewTextBoxColumn
            // 
            this.idcategoriaDataGridViewTextBoxColumn.DataPropertyName = "id_categoria";
            this.idcategoriaDataGridViewTextBoxColumn.DataSource = this.bindingSourceCategories;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.idcategoriaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.idcategoriaDataGridViewTextBoxColumn.DisplayMember = "nom";
            this.idcategoriaDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.idcategoriaDataGridViewTextBoxColumn.HeaderText = "Categoria";
            this.idcategoriaDataGridViewTextBoxColumn.Name = "idcategoriaDataGridViewTextBoxColumn";
            this.idcategoriaDataGridViewTextBoxColumn.ReadOnly = true;
            this.idcategoriaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.idcategoriaDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.idcategoriaDataGridViewTextBoxColumn.ValueMember = "id";
            this.idcategoriaDataGridViewTextBoxColumn.Width = 200;
            // 
            // bindingSourceCategories
            // 
            this.bindingSourceCategories.DataSource = typeof(Projecte2CSharp.categories);
            // 
            // idesportDataGridViewTextBoxColumn
            // 
            this.idesportDataGridViewTextBoxColumn.DataPropertyName = "id_esport";
            this.idesportDataGridViewTextBoxColumn.DataSource = this.bindingSourceEsport;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.idesportDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.idesportDataGridViewTextBoxColumn.DisplayMember = "nom";
            this.idesportDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.idesportDataGridViewTextBoxColumn.HeaderText = "Esport";
            this.idesportDataGridViewTextBoxColumn.Name = "idesportDataGridViewTextBoxColumn";
            this.idesportDataGridViewTextBoxColumn.ReadOnly = true;
            this.idesportDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.idesportDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.idesportDataGridViewTextBoxColumn.ValueMember = "id";
            this.idesportDataGridViewTextBoxColumn.Width = 150;
            // 
            // bindingSourceEsport
            // 
            this.bindingSourceEsport.DataSource = typeof(Projecte2CSharp.esports);
            // 
            // idsexeDataGridViewTextBoxColumn
            // 
            this.idsexeDataGridViewTextBoxColumn.DataPropertyName = "id_sexe";
            this.idsexeDataGridViewTextBoxColumn.DataSource = this.bindingSourceSexes;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.idsexeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.idsexeDataGridViewTextBoxColumn.DisplayMember = "nom";
            this.idsexeDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.idsexeDataGridViewTextBoxColumn.HeaderText = "Sexe";
            this.idsexeDataGridViewTextBoxColumn.Name = "idsexeDataGridViewTextBoxColumn";
            this.idsexeDataGridViewTextBoxColumn.ReadOnly = true;
            this.idsexeDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.idsexeDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.idsexeDataGridViewTextBoxColumn.ValueMember = "id";
            this.idsexeDataGridViewTextBoxColumn.Width = 150;
            // 
            // bindingSourceSexes
            // 
            this.bindingSourceSexes.DataSource = typeof(Projecte2CSharp.sexes);
            // 
            // idcompeticioDataGridViewTextBoxColumn
            // 
            this.idcompeticioDataGridViewTextBoxColumn.DataPropertyName = "id_competicio";
            this.idcompeticioDataGridViewTextBoxColumn.DataSource = this.bindingSourceCompeticions;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.idcompeticioDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.idcompeticioDataGridViewTextBoxColumn.DisplayMember = "nom";
            this.idcompeticioDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.idcompeticioDataGridViewTextBoxColumn.HeaderText = "Competició";
            this.idcompeticioDataGridViewTextBoxColumn.Name = "idcompeticioDataGridViewTextBoxColumn";
            this.idcompeticioDataGridViewTextBoxColumn.ReadOnly = true;
            this.idcompeticioDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.idcompeticioDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.idcompeticioDataGridViewTextBoxColumn.ValueMember = "id";
            this.idcompeticioDataGridViewTextBoxColumn.Width = 150;
            // 
            // bindingSourceCompeticions
            // 
            this.bindingSourceCompeticions.DataSource = typeof(Projecte2CSharp.competicions);
            // 
            // idcategoriacompeticioDataGridViewTextBoxColumn
            // 
            this.idcategoriacompeticioDataGridViewTextBoxColumn.DataPropertyName = "id_categoria_competicio";
            this.idcategoriacompeticioDataGridViewTextBoxColumn.HeaderText = "id_categoria_competicio";
            this.idcategoriacompeticioDataGridViewTextBoxColumn.Name = "idcategoriacompeticioDataGridViewTextBoxColumn";
            this.idcategoriacompeticioDataGridViewTextBoxColumn.ReadOnly = true;
            this.idcategoriacompeticioDataGridViewTextBoxColumn.Visible = false;
            // 
            // borratDataGridViewCheckBoxColumn
            // 
            this.borratDataGridViewCheckBoxColumn.DataPropertyName = "borrat";
            this.borratDataGridViewCheckBoxColumn.HeaderText = "borrat";
            this.borratDataGridViewCheckBoxColumn.Name = "borratDataGridViewCheckBoxColumn";
            this.borratDataGridViewCheckBoxColumn.ReadOnly = true;
            this.borratDataGridViewCheckBoxColumn.Visible = false;
            // 
            // bindingSourceEquips
            // 
            this.bindingSourceEquips.DataSource = typeof(Projecte2CSharp.equips);
            // 
            // EntitatDemandesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1067, 692);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewEquips);
            this.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "EntitatDemandesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EntitatDemandesForm";
            this.Load += new System.EventHandler(this.EntitatDemandesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEquips)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCategories)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEsport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceSexes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCompeticions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEquips)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewEquips;
        private System.Windows.Forms.BindingSource bindingSourceEquips;
        private System.Windows.Forms.BindingSource bindingSourceSexes;
        private System.Windows.Forms.BindingSource bindingSourceCategories;
        private System.Windows.Forms.BindingSource bindingSourceEsport;
        private System.Windows.Forms.BindingSource bindingSourceCompeticions;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn idcategoriaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn idesportDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn idsexeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn idcompeticioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idcategoriacompeticioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn borratDataGridViewCheckBoxColumn;
    }
}