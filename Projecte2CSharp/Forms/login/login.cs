﻿using Projecte2CSharp.Clases;
using Projecte2CSharp.Clases.Exceptions;
using Projecte2CSharp.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.Forms.login

{
    public partial class Login : Form
    {
        public Boolean canLogin { get; set; }
        public Login()
        {
            InitializeComponent();
        }

        private void textBoxUser_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxPassword_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            try
            {
                usuaris user = login();
                if (canLogin)
                {
                    MainForm mf = new MainForm(user);
                    mf.Show();
                    this.Hide();
                }
            }
            catch (NullUserException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }                
        } 
     

        public usuaris login() 
        {
            string msg = "";
            usuaris user = ORMUsuaris.selectUsuariByUsername(textBoxUser.Text, ref msg);
            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (user != null)
                {
                    String hashed = Hash.hashString(textBoxPassword.Text);
                    if (hashed.Equals(user.contrasenya))
                    {
                        canLogin = true;
                        if (checkBoxRememberUsername.Checked)
                        {
                            Properties.Settings.Default.username = textBoxUser.Text;
                            Properties.Settings.Default.Save();
                        }
                        else
                        {
                            Properties.Settings.Default.username = string.Empty;
                            Properties.Settings.Default.Save();
                        }

                    }
                    else
                    {
                        canLogin = false;
                        MessageBox.Show("Usuari o contrasenya incorrecte", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    throw new NullUserException();
                }
            }

            return user;
        }

        private void textBoxPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                login();
            }
        }

       

        private void Login_Load(object sender, EventArgs e)
        {
            //buttonLogin.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            if (Properties.Settings.Default.username != string.Empty)
            {
                textBoxUser.Text = Properties.Settings.Default.username;
                checkBoxRememberUsername.Checked = true;
                textBoxPassword.Select();
            }
        }

        private void Login_Activated(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.username != string.Empty)
            {
                textBoxUser.Text = Properties.Settings.Default.username;
                checkBoxRememberUsername.Checked = true;
                textBoxPassword.Select();
            }
            else
            {
                textBoxUser.Text = "";
            }
            textBoxPassword.Text = "";
        }
    }
}
