﻿namespace Projecte2CSharp.Forms
{
    partial class EquipForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EquipForm));
            this.buttonCancelar = new System.Windows.Forms.Button();
            this.buttonSaveChanges = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNomEquip = new System.Windows.Forms.TextBox();
            this.comboBoxCategoria = new System.Windows.Forms.ComboBox();
            this.bindingSourceCategories = new System.Windows.Forms.BindingSource(this.components);
            this.comboBoxSexe = new System.Windows.Forms.ComboBox();
            this.bindingSourceSexes = new System.Windows.Forms.BindingSource(this.components);
            this.labelNomEquip = new System.Windows.Forms.Label();
            this.k = new System.Windows.Forms.DataGridView();
            this.nomInstalacioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomEspaiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hora_inici = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hora_final = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourceHorarisComplets = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceHoraris = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceActivitatsProgramades = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceEspais = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceActivitats = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceInstalacions = new System.Windows.Forms.BindingSource(this.components);
            this.comboBoxEntitat = new System.Windows.Forms.ComboBox();
            this.bindingSourceEntitats = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxCompeticions = new System.Windows.Forms.ComboBox();
            this.bindingSourceCompeticions = new System.Windows.Forms.BindingSource(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxEsport = new System.Windows.Forms.ComboBox();
            this.bindingSourceEsports = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCategories)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceSexes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.k)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceHorarisComplets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceHoraris)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceActivitatsProgramades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEspais)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceActivitats)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceInstalacions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEntitats)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCompeticions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEsports)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonCancelar
            // 
            this.buttonCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(181)))));
            this.buttonCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancelar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonCancelar.FlatAppearance.BorderSize = 3;
            this.buttonCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancelar.Location = new System.Drawing.Point(711, 685);
            this.buttonCancelar.Name = "buttonCancelar";
            this.buttonCancelar.Size = new System.Drawing.Size(119, 51);
            this.buttonCancelar.TabIndex = 14;
            this.buttonCancelar.Text = "Cancel·lar";
            this.buttonCancelar.UseVisualStyleBackColor = false;
            this.buttonCancelar.Click += new System.EventHandler(this.buttonCancelar_Click);
            // 
            // buttonSaveChanges
            // 
            this.buttonSaveChanges.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonSaveChanges.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonSaveChanges.FlatAppearance.BorderSize = 3;
            this.buttonSaveChanges.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSaveChanges.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonSaveChanges.Location = new System.Drawing.Point(857, 685);
            this.buttonSaveChanges.Name = "buttonSaveChanges";
            this.buttonSaveChanges.Size = new System.Drawing.Size(186, 51);
            this.buttonSaveChanges.TabIndex = 13;
            this.buttonSaveChanges.Text = "Guardar canvis";
            this.buttonSaveChanges.UseVisualStyleBackColor = false;
            this.buttonSaveChanges.Click += new System.EventHandler(this.buttonSaveChanges_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 20);
            this.label1.TabIndex = 15;
            this.label1.Text = "Nom*";
            // 
            // textBoxNomEquip
            // 
            this.textBoxNomEquip.Location = new System.Drawing.Point(128, 89);
            this.textBoxNomEquip.Name = "textBoxNomEquip";
            this.textBoxNomEquip.Size = new System.Drawing.Size(242, 28);
            this.textBoxNomEquip.TabIndex = 16;
            this.textBoxNomEquip.TextChanged += new System.EventHandler(this.textBoxNomEquip_TextChanged);
            // 
            // comboBoxCategoria
            // 
            this.comboBoxCategoria.DataSource = this.bindingSourceCategories;
            this.comboBoxCategoria.DisplayMember = "nom";
            this.comboBoxCategoria.FormattingEnabled = true;
            this.comboBoxCategoria.Location = new System.Drawing.Point(128, 221);
            this.comboBoxCategoria.Name = "comboBoxCategoria";
            this.comboBoxCategoria.Size = new System.Drawing.Size(242, 28);
            this.comboBoxCategoria.TabIndex = 17;
            this.comboBoxCategoria.ValueMember = "id";
            // 
            // bindingSourceCategories
            // 
            this.bindingSourceCategories.DataSource = typeof(Projecte2CSharp.categories);
            // 
            // comboBoxSexe
            // 
            this.comboBoxSexe.DataSource = this.bindingSourceSexes;
            this.comboBoxSexe.DisplayMember = "nom";
            this.comboBoxSexe.FormattingEnabled = true;
            this.comboBoxSexe.Location = new System.Drawing.Point(128, 262);
            this.comboBoxSexe.Name = "comboBoxSexe";
            this.comboBoxSexe.Size = new System.Drawing.Size(242, 28);
            this.comboBoxSexe.TabIndex = 20;
            this.comboBoxSexe.ValueMember = "id";
            // 
            // bindingSourceSexes
            // 
            this.bindingSourceSexes.DataSource = typeof(Projecte2CSharp.sexes);
            // 
            // labelNomEquip
            // 
            this.labelNomEquip.AutoSize = true;
            this.labelNomEquip.Font = new System.Drawing.Font("Source Sans Pro", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNomEquip.Location = new System.Drawing.Point(35, 28);
            this.labelNomEquip.Name = "labelNomEquip";
            this.labelNomEquip.Size = new System.Drawing.Size(126, 31);
            this.labelNomEquip.TabIndex = 21;
            this.labelNomEquip.Text = "NomEquip";
            // 
            // k
            // 
            this.k.AllowUserToAddRows = false;
            this.k.AllowUserToResizeColumns = false;
            this.k.AllowUserToResizeRows = false;
            this.k.AutoGenerateColumns = false;
            this.k.BackgroundColor = System.Drawing.Color.White;
            this.k.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.k.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.k.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.k.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.k.ColumnHeadersHeight = 35;
            this.k.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nomInstalacioDataGridViewTextBoxColumn,
            this.nomEspaiDataGridViewTextBoxColumn,
            this.tipus,
            this.dia,
            this.hora_inici,
            this.hora_final});
            this.k.DataSource = this.bindingSourceHorarisComplets;
            this.k.EnableHeadersVisualStyles = false;
            this.k.Location = new System.Drawing.Point(41, 439);
            this.k.MultiSelect = false;
            this.k.Name = "k";
            this.k.ReadOnly = true;
            this.k.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.k.RowHeadersVisible = false;
            this.k.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.k.RowTemplate.Height = 24;
            this.k.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.k.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.k.Size = new System.Drawing.Size(1002, 215);
            this.k.TabIndex = 22;
            // 
            // nomInstalacioDataGridViewTextBoxColumn
            // 
            this.nomInstalacioDataGridViewTextBoxColumn.DataPropertyName = "nomInstalacio";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.nomInstalacioDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.nomInstalacioDataGridViewTextBoxColumn.HeaderText = "Instal·lació";
            this.nomInstalacioDataGridViewTextBoxColumn.Name = "nomInstalacioDataGridViewTextBoxColumn";
            this.nomInstalacioDataGridViewTextBoxColumn.ReadOnly = true;
            this.nomInstalacioDataGridViewTextBoxColumn.Width = 200;
            // 
            // nomEspaiDataGridViewTextBoxColumn
            // 
            this.nomEspaiDataGridViewTextBoxColumn.DataPropertyName = "nomEspai";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.nomEspaiDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.nomEspaiDataGridViewTextBoxColumn.HeaderText = "Espai";
            this.nomEspaiDataGridViewTextBoxColumn.Name = "nomEspaiDataGridViewTextBoxColumn";
            this.nomEspaiDataGridViewTextBoxColumn.ReadOnly = true;
            this.nomEspaiDataGridViewTextBoxColumn.Width = 200;
            // 
            // tipus
            // 
            this.tipus.DataPropertyName = "tipus";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.tipus.DefaultCellStyle = dataGridViewCellStyle4;
            this.tipus.HeaderText = "Tipus";
            this.tipus.Name = "tipus";
            this.tipus.ReadOnly = true;
            this.tipus.Width = 150;
            // 
            // dia
            // 
            this.dia.DataPropertyName = "dia";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dia.DefaultCellStyle = dataGridViewCellStyle5;
            this.dia.HeaderText = "Dia";
            this.dia.Name = "dia";
            this.dia.ReadOnly = true;
            // 
            // hora_inici
            // 
            this.hora_inici.DataPropertyName = "hora_inici";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.hora_inici.DefaultCellStyle = dataGridViewCellStyle6;
            this.hora_inici.HeaderText = "Inici";
            this.hora_inici.Name = "hora_inici";
            this.hora_inici.ReadOnly = true;
            // 
            // hora_final
            // 
            this.hora_final.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.hora_final.DataPropertyName = "hora_final";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.hora_final.DefaultCellStyle = dataGridViewCellStyle7;
            this.hora_final.HeaderText = "Fi";
            this.hora_final.Name = "hora_final";
            this.hora_final.ReadOnly = true;
            // 
            // bindingSourceHorarisComplets
            // 
            this.bindingSourceHorarisComplets.DataSource = typeof(Projecte2CSharp.Clases.POJO.HorariComplet);
            // 
            // bindingSourceHoraris
            // 
            this.bindingSourceHoraris.DataSource = typeof(Projecte2CSharp.horaris_activitats);
            // 
            // bindingSourceActivitatsProgramades
            // 
            this.bindingSourceActivitatsProgramades.DataSource = typeof(Projecte2CSharp.Clases.POJO.ActivitatConcedida);
            // 
            // bindingSourceEspais
            // 
            this.bindingSourceEspais.DataSource = typeof(Projecte2CSharp.espais);
            // 
            // bindingSourceActivitats
            // 
            this.bindingSourceActivitats.DataSource = typeof(Projecte2CSharp.activitats_concedides);
            // 
            // comboBoxEntitat
            // 
            this.comboBoxEntitat.DataSource = this.bindingSourceEntitats;
            this.comboBoxEntitat.DisplayMember = "nom";
            this.comboBoxEntitat.FormattingEnabled = true;
            this.comboBoxEntitat.Location = new System.Drawing.Point(128, 134);
            this.comboBoxEntitat.Name = "comboBoxEntitat";
            this.comboBoxEntitat.Size = new System.Drawing.Size(242, 28);
            this.comboBoxEntitat.TabIndex = 24;
            this.comboBoxEntitat.ValueMember = "id";
            // 
            // bindingSourceEntitats
            // 
            this.bindingSourceEntitats.DataSource = typeof(Projecte2CSharp.entitats);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 20);
            this.label4.TabIndex = 25;
            this.label4.Text = "Entitat*";
            // 
            // comboBoxCompeticions
            // 
            this.comboBoxCompeticions.DataSource = this.bindingSourceCompeticions;
            this.comboBoxCompeticions.DisplayMember = "nom";
            this.comboBoxCompeticions.FormattingEnabled = true;
            this.comboBoxCompeticions.Location = new System.Drawing.Point(128, 304);
            this.comboBoxCompeticions.Name = "comboBoxCompeticions";
            this.comboBoxCompeticions.Size = new System.Drawing.Size(242, 28);
            this.comboBoxCompeticions.TabIndex = 27;
            this.comboBoxCompeticions.ValueMember = "id";
            // 
            // bindingSourceCompeticions
            // 
            this.bindingSourceCompeticions.DataSource = typeof(Projecte2CSharp.competicions);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(37, 224);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 20);
            this.label8.TabIndex = 18;
            this.label8.Text = "Categoria*";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(37, 265);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 20);
            this.label9.TabIndex = 19;
            this.label9.Text = "Sexe*";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(37, 307);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 20);
            this.label10.TabIndex = 26;
            this.label10.Text = "Competició*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 181);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 20);
            this.label2.TabIndex = 28;
            this.label2.Text = "Esport*";
            // 
            // comboBoxEsport
            // 
            this.comboBoxEsport.DataSource = this.bindingSourceEsports;
            this.comboBoxEsport.DisplayMember = "nom";
            this.comboBoxEsport.FormattingEnabled = true;
            this.comboBoxEsport.Location = new System.Drawing.Point(128, 178);
            this.comboBoxEsport.Name = "comboBoxEsport";
            this.comboBoxEsport.Size = new System.Drawing.Size(242, 28);
            this.comboBoxEsport.TabIndex = 29;
            this.comboBoxEsport.ValueMember = "id";
            // 
            // bindingSourceEsports
            // 
            this.bindingSourceEsports.DataSource = typeof(Projecte2CSharp.esports);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 398);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 20);
            this.label3.TabIndex = 30;
            this.label3.Text = "Activitats programades";
            // 
            // EquipForm
            // 
            this.AcceptButton = this.buttonSaveChanges;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.buttonCancelar;
            this.ClientSize = new System.Drawing.Size(1071, 758);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBoxEsport);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxCompeticions);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBoxEntitat);
            this.Controls.Add(this.k);
            this.Controls.Add(this.labelNomEquip);
            this.Controls.Add(this.comboBoxSexe);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBoxCategoria);
            this.Controls.Add(this.textBoxNomEquip);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCancelar);
            this.Controls.Add(this.buttonSaveChanges);
            this.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "EquipForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EquipForm";
            this.Load += new System.EventHandler(this.EquipForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCategories)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceSexes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.k)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceHorarisComplets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceHoraris)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceActivitatsProgramades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEspais)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceActivitats)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceInstalacions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEntitats)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCompeticions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEsports)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCancelar;
        private System.Windows.Forms.Button buttonSaveChanges;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNomEquip;
        private System.Windows.Forms.ComboBox comboBoxCategoria;
        private System.Windows.Forms.BindingSource bindingSourceCategories;
        private System.Windows.Forms.ComboBox comboBoxSexe;
        private System.Windows.Forms.BindingSource bindingSourceSexes;
        private System.Windows.Forms.Label labelNomEquip;
        private System.Windows.Forms.DataGridView k;
        private System.Windows.Forms.BindingSource bindingSourceActivitats;
        private System.Windows.Forms.BindingSource bindingSourceInstalacions;
        private System.Windows.Forms.BindingSource bindingSourceHoraris;
        private System.Windows.Forms.BindingSource bindingSourceEspais;
        private System.Windows.Forms.ComboBox comboBoxEntitat;
        private System.Windows.Forms.BindingSource bindingSourceEntitats;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxCompeticions;
        private System.Windows.Forms.BindingSource bindingSourceCompeticions;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxEsport;
        private System.Windows.Forms.BindingSource bindingSourceEsports;
        private System.Windows.Forms.BindingSource bindingSourceHorarisComplets;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.BindingSource bindingSourceActivitatsProgramades;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomInstalacioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomEspaiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipus;
        private System.Windows.Forms.DataGridViewTextBoxColumn dia;
        private System.Windows.Forms.DataGridViewTextBoxColumn hora_inici;
        private System.Windows.Forms.DataGridViewTextBoxColumn hora_final;
    }
}