﻿using Projecte2CSharp.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.Forms
{
    public partial class NewEspaiForm : Form
    {
        public espais espai { get; set; }
        public bool isModification { get; set; }
        public int id_instalacio { get; set; }
        public string IMG_PATH = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\..\\recursos\\imagenes\\"));
        public string imatge { get; set; }

        private instalacions instalacio;
        public NewEspaiForm(instalacions instalacio)
        {
            this.instalacio = instalacio;
            InitializeComponent();
        }

        public NewEspaiForm(espais espai, int id)
        {
            this.id_instalacio = id;
            this.espai = espai;
            isModification = true;
            InitializeComponent();
        }


        private void NewEspaiForm_Load(object sender, EventArgs e)
        {
            //modificacio
            if (isModification)
            {
                if (espai.imatge != null)
                {
                   string img  = IMG_PATH + espai.imatge + ".jpeg";
                    pictureBoxImg.Image = new Bitmap(img);
                    imatge = espai.imatge;
                }
                this.Text = espai.nom;
                textBoxNom.Text = espai.nom;
                textBoxPreu.Text = espai.preu.ToString();
                checkBox1.Checked = (bool)espai.es_exterior ? true : false;
            }
            //alta
            else
            {
                this.Text = "Nou espai";
            }
        }

        private void buttonNovaImatge_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();

            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";

            if (open.ShowDialog() == DialogResult.OK)
            {
                Bitmap img = new Bitmap(open.FileName);
                string filename = generateFileName();
                pictureBoxImg.Image = img;
                //string path = "C:\\Users\\Ferran\\Desktop\\img\\"+ "hoooola" + ".jpeg";
                string path = IMG_PATH + filename + ".jpeg";

                //img.Save(@"C:\\Users\\Ferran\Desktop\\img");
                //pictureBoxImg.Image.Save(path, ImageFormat.Jpeg);

                pictureBoxImg.Image.Save(path);
                //File.Delete(path);
                //img.Save(@Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\..\\recursos\\imagenes")));
                imatge = filename;
            }
        }

        public string generateFileName()
        {
            string fileName;
            DateTime today = DateTime.Now;
            fileName = today.ToString("ddMMyyyyhhss") + "_" + id_instalacio + "_" + textBoxNom.Text.Replace(" ", "_");
            return fileName;
        }

        private void buttonSaveChanges_Click(object sender, EventArgs e)
        {
            if (!isModification) { espai = new espais(); }

            //asociacio de camps
            espai.nom = textBoxNom.Text;
            //afegir regex
            espai.preu = Decimal.Parse(textBoxPreu.Text);
            espai.es_exterior = checkBox1.Checked ? true : false;
            espai.imatge = imatge;
            espai.id_instalacio = id_instalacio;


            if (isModification)
            {
                string msg = ORMEspais.updateEspai(espai);

                if (!msg.Equals(""))
                {
                    MessageBox.Show("Algo ha anat malament quan es guardaven els canvis", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                instalacio.espais.Add(espai);
            }
            this.Close();
        }

        private void textBoxNom_TextChanged(object sender, EventArgs e)
        {
            labelNouEspai.Text = textBoxNom.Text;
        }
    }
}
