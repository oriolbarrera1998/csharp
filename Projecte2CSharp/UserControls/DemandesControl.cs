﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projecte2CSharp.DB;
using Projecte2CSharp.Clases.POJO;
using Projecte2CSharp.Forms;

namespace Projecte2CSharp.UserControls
{
    public partial class DemandesControl : UserControl
    {
        public DemandesControl()
        {
            InitializeComponent();
        }

        private void textBoxBusqueda_TextChanged(object sender, EventArgs e)
        {
            string msg = "";
            List<activitats_demanades> l = null;
            if (radioButtonSearchInstalacio.Checked)
            {
                l = ORMActivitatsConcedides.getActivitatsDemanadesByInstalacio(ref msg, textBoxBusqueda.Text);
            }
            else if (radioButtonSearchEntitat.Checked)
            {
                l = ORMActivitatsConcedides.getActivitatsDemanadesByEntitat(ref msg, textBoxBusqueda.Text);
            }
            bindingSourceDemandes.DataSource = ActivitatDemanada.createDemandes(l);
        }

        private void DemandesControl_Load(object sender, EventArgs e)
        {
            string msg = "";
            bindingSourceDemandes.DataSource = ActivitatDemanada.createDemandes(ORMActivitatsConcedides.getActivitatsDemanades(ref msg));
            bindingSourceEquips.DataSource = ORMEquips.selectEquips(ref msg);
            bindingSourceEspais.DataSource = ORMEspais.selectEspais(ref msg);

            radioButtonSearchInstalacio.Checked = true;

            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridViewDemandes_DoubleClick(object sender, EventArgs e)
        {
            FormDemanda fd = new FormDemanda((ActivitatDemanada)dataGridViewDemandes.CurrentRow.DataBoundItem, MainForm.user.id);
            fd.ShowDialog();
        }

        

        private void buttonModificar_Click(object sender, EventArgs e)
        {
            FormDemanda fd = new FormDemanda((ActivitatDemanada)dataGridViewDemandes.CurrentRow.DataBoundItem, MainForm.user.id);
            fd.ShowDialog();

        }

        public void setSearchText(string s)
        {
            textBoxBusqueda.Text = s;
        }
    }
}
