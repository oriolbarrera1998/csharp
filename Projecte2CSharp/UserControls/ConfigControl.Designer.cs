﻿namespace Projecte2CSharp.UserControls
{
    partial class ConfigControl
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridViewEsports = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCategories = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewUsuaris = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonNovaTemporada = new System.Windows.Forms.Button();
            this.labelTemporada = new System.Windows.Forms.Label();
            this.textBoxTemporada = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonEliminarCategoria = new System.Windows.Forms.Button();
            this.buttonEditCategoria = new System.Windows.Forms.Button();
            this.buttonAddCategoria = new System.Windows.Forms.Button();
            this.buttonEliminarUsuari = new System.Windows.Forms.Button();
            this.buttonEditUsuari = new System.Windows.Forms.Button();
            this.buttonAddUsuari = new System.Windows.Forms.Button();
            this.buttonEliminarEsport = new System.Windows.Forms.Button();
            this.buttonEditEsport = new System.Windows.Forms.Button();
            this.buttonAddEsport = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.labelNewPassword = new System.Windows.Forms.Label();
            this.buttonCanviarPassword = new System.Windows.Forms.Button();
            this.textBoxNewPassword = new System.Windows.Forms.TextBox();
            this.labelConfirmNewPassword = new System.Windows.Forms.Label();
            this.textBoxConfirmNewPassword = new System.Windows.Forms.TextBox();
            this.labelActualPassword = new System.Windows.Forms.Label();
            this.textBoxActualPassword = new System.Windows.Forms.TextBox();
            this.idDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomUsuariDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.correuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contrasenyaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activitatsconcedidesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourceUsuaris = new System.Windows.Forms.BindingSource(this.components);
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.equipsDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourceCategories = new System.Windows.Forms.BindingSource(this.components);
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.equipsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourceEsports = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceSexes = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEsports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCategories)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUsuaris)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceUsuaris)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCategories)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEsports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceSexes)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewEsports
            // 
            this.dataGridViewEsports.AllowUserToAddRows = false;
            this.dataGridViewEsports.AutoGenerateColumns = false;
            this.dataGridViewEsports.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewEsports.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewEsports.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridViewEsports.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewEsports.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewEsports.ColumnHeadersHeight = 35;
            this.dataGridViewEsports.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewEsports.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.idDataGridViewTextBoxColumn,
            this.nomDataGridViewTextBoxColumn,
            this.equipsDataGridViewTextBoxColumn});
            this.dataGridViewEsports.DataSource = this.bindingSourceEsports;
            this.dataGridViewEsports.EnableHeadersVisualStyles = false;
            this.dataGridViewEsports.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.dataGridViewEsports.Location = new System.Drawing.Point(25, 115);
            this.dataGridViewEsports.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dataGridViewEsports.Name = "dataGridViewEsports";
            this.dataGridViewEsports.ReadOnly = true;
            this.dataGridViewEsports.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewEsports.RowHeadersVisible = false;
            this.dataGridViewEsports.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewEsports.RowTemplate.Height = 35;
            this.dataGridViewEsports.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewEsports.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewEsports.Size = new System.Drawing.Size(488, 232);
            this.dataGridViewEsports.TabIndex = 1;
            this.dataGridViewEsports.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridViewEsports_UserDeletingRow);
            this.dataGridViewEsports.DoubleClick += new System.EventHandler(this.dataGridViewEsports_DoubleClick);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.DataPropertyName = "id";
            this.Column1.HeaderText = "id";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // dataGridViewCategories
            // 
            this.dataGridViewCategories.AllowUserToAddRows = false;
            this.dataGridViewCategories.AutoGenerateColumns = false;
            this.dataGridViewCategories.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewCategories.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewCategories.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridViewCategories.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCategories.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewCategories.ColumnHeadersHeight = 35;
            this.dataGridViewCategories.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewCategories.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.idDataGridViewTextBoxColumn1,
            this.nomDataGridViewTextBoxColumn1,
            this.equipsDataGridViewTextBoxColumn1});
            this.dataGridViewCategories.DataSource = this.bindingSourceCategories;
            this.dataGridViewCategories.EnableHeadersVisualStyles = false;
            this.dataGridViewCategories.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.dataGridViewCategories.Location = new System.Drawing.Point(587, 115);
            this.dataGridViewCategories.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dataGridViewCategories.Name = "dataGridViewCategories";
            this.dataGridViewCategories.ReadOnly = true;
            this.dataGridViewCategories.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewCategories.RowHeadersVisible = false;
            this.dataGridViewCategories.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewCategories.RowTemplate.Height = 35;
            this.dataGridViewCategories.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCategories.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewCategories.Size = new System.Drawing.Size(488, 232);
            this.dataGridViewCategories.TabIndex = 2;
            this.dataGridViewCategories.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridViewCategories_UserDeletingRow);
            this.dataGridViewCategories.DoubleClick += new System.EventHandler(this.dataGridViewCategories_DoubleClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn1.HeaderText = "id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewUsuaris
            // 
            this.dataGridViewUsuaris.AllowUserToAddRows = false;
            this.dataGridViewUsuaris.AutoGenerateColumns = false;
            this.dataGridViewUsuaris.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewUsuaris.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewUsuaris.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridViewUsuaris.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewUsuaris.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewUsuaris.ColumnHeadersHeight = 35;
            this.dataGridViewUsuaris.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewUsuaris.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.idDataGridViewTextBoxColumn2,
            this.nomDataGridViewTextBoxColumn2,
            this.nomUsuariDataGridViewTextBoxColumn,
            this.correuDataGridViewTextBoxColumn,
            this.contrasenyaDataGridViewTextBoxColumn,
            this.activitatsconcedidesDataGridViewTextBoxColumn});
            this.dataGridViewUsuaris.DataSource = this.bindingSourceUsuaris;
            this.dataGridViewUsuaris.EnableHeadersVisualStyles = false;
            this.dataGridViewUsuaris.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.dataGridViewUsuaris.Location = new System.Drawing.Point(25, 438);
            this.dataGridViewUsuaris.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dataGridViewUsuaris.Name = "dataGridViewUsuaris";
            this.dataGridViewUsuaris.ReadOnly = true;
            this.dataGridViewUsuaris.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewUsuaris.RowHeadersVisible = false;
            this.dataGridViewUsuaris.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewUsuaris.RowTemplate.Height = 35;
            this.dataGridViewUsuaris.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewUsuaris.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewUsuaris.Size = new System.Drawing.Size(488, 278);
            this.dataGridViewUsuaris.TabIndex = 4;
            this.dataGridViewUsuaris.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridViewUsuaris_UserDeletingRow);
            this.dataGridViewUsuaris.DoubleClick += new System.EventHandler(this.dataGridViewUsuaris_DoubleClick);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn3.HeaderText = "id";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Source Sans Pro", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 24);
            this.label1.TabIndex = 5;
            this.label1.Text = "Esports";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Source Sans Pro", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(583, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 24);
            this.label2.TabIndex = 6;
            this.label2.Text = "Categories";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Source Sans Pro", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(21, 398);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 24);
            this.label4.TabIndex = 8;
            this.label4.Text = "Usuaris";
            // 
            // buttonNovaTemporada
            // 
            this.buttonNovaTemporada.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonNovaTemporada.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonNovaTemporada.FlatAppearance.BorderSize = 3;
            this.buttonNovaTemporada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNovaTemporada.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonNovaTemporada.Location = new System.Drawing.Point(587, 455);
            this.buttonNovaTemporada.Name = "buttonNovaTemporada";
            this.buttonNovaTemporada.Size = new System.Drawing.Size(486, 33);
            this.buttonNovaTemporada.TabIndex = 39;
            this.buttonNovaTemporada.Text = "Generar nova temporada";
            this.buttonNovaTemporada.UseVisualStyleBackColor = false;
            this.buttonNovaTemporada.Click += new System.EventHandler(this.buttonNovaTemporada_Click);
            // 
            // labelTemporada
            // 
            this.labelTemporada.AutoSize = true;
            this.labelTemporada.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTemporada.Location = new System.Drawing.Point(587, 424);
            this.labelTemporada.Name = "labelTemporada";
            this.labelTemporada.Size = new System.Drawing.Size(230, 20);
            this.labelTemporada.TabIndex = 40;
            this.labelTemporada.Text = "Nom de la nova nova temporada:";
            // 
            // textBoxTemporada
            // 
            this.textBoxTemporada.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTemporada.Location = new System.Drawing.Point(856, 421);
            this.textBoxTemporada.Name = "textBoxTemporada";
            this.textBoxTemporada.Size = new System.Drawing.Size(217, 28);
            this.textBoxTemporada.TabIndex = 41;
            this.textBoxTemporada.Text = "exemple: 2018-2019";
            this.textBoxTemporada.Click += new System.EventHandler(this.textBoxTemporada_Click);
            this.textBoxTemporada.TextChanged += new System.EventHandler(this.textBoxTemporada_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Source Sans Pro", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(587, 384);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 24);
            this.label3.TabIndex = 42;
            this.label3.Text = "Nova temporada";
            // 
            // buttonEliminarCategoria
            // 
            this.buttonEliminarCategoria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(181)))));
            this.buttonEliminarCategoria.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonEliminarCategoria.FlatAppearance.BorderSize = 3;
            this.buttonEliminarCategoria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEliminarCategoria.Image = global::Projecte2CSharp.Properties.Resources.trash16;
            this.buttonEliminarCategoria.Location = new System.Drawing.Point(806, 69);
            this.buttonEliminarCategoria.Name = "buttonEliminarCategoria";
            this.buttonEliminarCategoria.Size = new System.Drawing.Size(42, 33);
            this.buttonEliminarCategoria.TabIndex = 38;
            this.buttonEliminarCategoria.UseVisualStyleBackColor = false;
            this.buttonEliminarCategoria.Click += new System.EventHandler(this.buttonEliminarCategoria_Click);
            // 
            // buttonEditCategoria
            // 
            this.buttonEditCategoria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(182)))));
            this.buttonEditCategoria.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonEditCategoria.FlatAppearance.BorderSize = 3;
            this.buttonEditCategoria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEditCategoria.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonEditCategoria.Image = global::Projecte2CSharp.Properties.Resources.edit16;
            this.buttonEditCategoria.Location = new System.Drawing.Point(758, 69);
            this.buttonEditCategoria.Name = "buttonEditCategoria";
            this.buttonEditCategoria.Size = new System.Drawing.Size(42, 33);
            this.buttonEditCategoria.TabIndex = 37;
            this.buttonEditCategoria.UseVisualStyleBackColor = false;
            this.buttonEditCategoria.Click += new System.EventHandler(this.buttonEditCategoria_Click);
            // 
            // buttonAddCategoria
            // 
            this.buttonAddCategoria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonAddCategoria.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonAddCategoria.FlatAppearance.BorderSize = 3;
            this.buttonAddCategoria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddCategoria.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonAddCategoria.Image = global::Projecte2CSharp.Properties.Resources.add16;
            this.buttonAddCategoria.Location = new System.Drawing.Point(710, 69);
            this.buttonAddCategoria.Name = "buttonAddCategoria";
            this.buttonAddCategoria.Size = new System.Drawing.Size(42, 33);
            this.buttonAddCategoria.TabIndex = 36;
            this.buttonAddCategoria.UseVisualStyleBackColor = false;
            this.buttonAddCategoria.Click += new System.EventHandler(this.buttonAddCategoria_Click);
            // 
            // buttonEliminarUsuari
            // 
            this.buttonEliminarUsuari.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(181)))));
            this.buttonEliminarUsuari.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonEliminarUsuari.FlatAppearance.BorderSize = 3;
            this.buttonEliminarUsuari.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEliminarUsuari.Image = global::Projecte2CSharp.Properties.Resources.trash16;
            this.buttonEliminarUsuari.Location = new System.Drawing.Point(210, 392);
            this.buttonEliminarUsuari.Name = "buttonEliminarUsuari";
            this.buttonEliminarUsuari.Size = new System.Drawing.Size(42, 33);
            this.buttonEliminarUsuari.TabIndex = 35;
            this.buttonEliminarUsuari.UseVisualStyleBackColor = false;
            this.buttonEliminarUsuari.Click += new System.EventHandler(this.buttonEliminarUsuari_Click);
            // 
            // buttonEditUsuari
            // 
            this.buttonEditUsuari.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(182)))));
            this.buttonEditUsuari.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonEditUsuari.FlatAppearance.BorderSize = 3;
            this.buttonEditUsuari.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEditUsuari.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonEditUsuari.Image = global::Projecte2CSharp.Properties.Resources.edit16;
            this.buttonEditUsuari.Location = new System.Drawing.Point(161, 392);
            this.buttonEditUsuari.Name = "buttonEditUsuari";
            this.buttonEditUsuari.Size = new System.Drawing.Size(42, 33);
            this.buttonEditUsuari.TabIndex = 34;
            this.buttonEditUsuari.UseVisualStyleBackColor = false;
            this.buttonEditUsuari.Click += new System.EventHandler(this.buttonEditUsuari_Click);
            // 
            // buttonAddUsuari
            // 
            this.buttonAddUsuari.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonAddUsuari.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonAddUsuari.FlatAppearance.BorderSize = 3;
            this.buttonAddUsuari.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddUsuari.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonAddUsuari.Image = global::Projecte2CSharp.Properties.Resources.add16;
            this.buttonAddUsuari.Location = new System.Drawing.Point(113, 392);
            this.buttonAddUsuari.Name = "buttonAddUsuari";
            this.buttonAddUsuari.Size = new System.Drawing.Size(42, 33);
            this.buttonAddUsuari.TabIndex = 33;
            this.buttonAddUsuari.UseVisualStyleBackColor = false;
            this.buttonAddUsuari.Click += new System.EventHandler(this.buttonAddUsuari_Click);
            // 
            // buttonEliminarEsport
            // 
            this.buttonEliminarEsport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(181)))));
            this.buttonEliminarEsport.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonEliminarEsport.FlatAppearance.BorderSize = 3;
            this.buttonEliminarEsport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEliminarEsport.Image = global::Projecte2CSharp.Properties.Resources.trash16;
            this.buttonEliminarEsport.Location = new System.Drawing.Point(208, 69);
            this.buttonEliminarEsport.Name = "buttonEliminarEsport";
            this.buttonEliminarEsport.Size = new System.Drawing.Size(42, 33);
            this.buttonEliminarEsport.TabIndex = 29;
            this.buttonEliminarEsport.UseVisualStyleBackColor = false;
            this.buttonEliminarEsport.Click += new System.EventHandler(this.buttonEliminarEsport_Click);
            // 
            // buttonEditEsport
            // 
            this.buttonEditEsport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(182)))));
            this.buttonEditEsport.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonEditEsport.FlatAppearance.BorderSize = 3;
            this.buttonEditEsport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEditEsport.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonEditEsport.Image = global::Projecte2CSharp.Properties.Resources.edit16;
            this.buttonEditEsport.Location = new System.Drawing.Point(160, 69);
            this.buttonEditEsport.Name = "buttonEditEsport";
            this.buttonEditEsport.Size = new System.Drawing.Size(42, 33);
            this.buttonEditEsport.TabIndex = 28;
            this.buttonEditEsport.UseVisualStyleBackColor = false;
            this.buttonEditEsport.Click += new System.EventHandler(this.buttonEditEsport_Click);
            // 
            // buttonAddEsport
            // 
            this.buttonAddEsport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonAddEsport.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonAddEsport.FlatAppearance.BorderSize = 3;
            this.buttonAddEsport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddEsport.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonAddEsport.Image = global::Projecte2CSharp.Properties.Resources.add16;
            this.buttonAddEsport.Location = new System.Drawing.Point(111, 69);
            this.buttonAddEsport.Name = "buttonAddEsport";
            this.buttonAddEsport.Size = new System.Drawing.Size(42, 33);
            this.buttonAddEsport.TabIndex = 27;
            this.buttonAddEsport.UseVisualStyleBackColor = false;
            this.buttonAddEsport.Click += new System.EventHandler(this.buttonAddEsport_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Source Sans Pro", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(587, 509);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(182, 24);
            this.label5.TabIndex = 46;
            this.label5.Text = "Canviar contrasenya";
            // 
            // labelNewPassword
            // 
            this.labelNewPassword.AutoSize = true;
            this.labelNewPassword.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewPassword.Location = new System.Drawing.Point(587, 596);
            this.labelNewPassword.Name = "labelNewPassword";
            this.labelNewPassword.Size = new System.Drawing.Size(129, 20);
            this.labelNewPassword.TabIndex = 44;
            this.labelNewPassword.Text = "Nova contrasenya";
            // 
            // buttonCanviarPassword
            // 
            this.buttonCanviarPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonCanviarPassword.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonCanviarPassword.FlatAppearance.BorderSize = 3;
            this.buttonCanviarPassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCanviarPassword.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonCanviarPassword.Location = new System.Drawing.Point(587, 669);
            this.buttonCanviarPassword.Name = "buttonCanviarPassword";
            this.buttonCanviarPassword.Size = new System.Drawing.Size(486, 33);
            this.buttonCanviarPassword.TabIndex = 43;
            this.buttonCanviarPassword.Text = "Canviar contrasenya";
            this.buttonCanviarPassword.UseVisualStyleBackColor = false;
            this.buttonCanviarPassword.Click += new System.EventHandler(this.buttonCanviarPassword_Click);
            // 
            // textBoxNewPassword
            // 
            this.textBoxNewPassword.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewPassword.Location = new System.Drawing.Point(832, 593);
            this.textBoxNewPassword.Name = "textBoxNewPassword";
            this.textBoxNewPassword.Size = new System.Drawing.Size(241, 28);
            this.textBoxNewPassword.TabIndex = 47;
            this.textBoxNewPassword.UseSystemPasswordChar = true;
            // 
            // labelConfirmNewPassword
            // 
            this.labelConfirmNewPassword.AutoSize = true;
            this.labelConfirmNewPassword.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConfirmNewPassword.Location = new System.Drawing.Point(587, 631);
            this.labelConfirmNewPassword.Name = "labelConfirmNewPassword";
            this.labelConfirmNewPassword.Size = new System.Drawing.Size(209, 20);
            this.labelConfirmNewPassword.TabIndex = 48;
            this.labelConfirmNewPassword.Text = "Confirma la nova contrasenya";
            // 
            // textBoxConfirmNewPassword
            // 
            this.textBoxConfirmNewPassword.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxConfirmNewPassword.Location = new System.Drawing.Point(832, 628);
            this.textBoxConfirmNewPassword.Name = "textBoxConfirmNewPassword";
            this.textBoxConfirmNewPassword.Size = new System.Drawing.Size(241, 28);
            this.textBoxConfirmNewPassword.TabIndex = 49;
            this.textBoxConfirmNewPassword.UseSystemPasswordChar = true;
            // 
            // labelActualPassword
            // 
            this.labelActualPassword.AutoSize = true;
            this.labelActualPassword.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActualPassword.Location = new System.Drawing.Point(587, 559);
            this.labelActualPassword.Name = "labelActualPassword";
            this.labelActualPassword.Size = new System.Drawing.Size(145, 20);
            this.labelActualPassword.TabIndex = 50;
            this.labelActualPassword.Text = "Constrasenya actual";
            // 
            // textBoxActualPassword
            // 
            this.textBoxActualPassword.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxActualPassword.Location = new System.Drawing.Point(832, 556);
            this.textBoxActualPassword.Name = "textBoxActualPassword";
            this.textBoxActualPassword.Size = new System.Drawing.Size(241, 28);
            this.textBoxActualPassword.TabIndex = 51;
            this.textBoxActualPassword.UseSystemPasswordChar = true;
            // 
            // idDataGridViewTextBoxColumn2
            // 
            this.idDataGridViewTextBoxColumn2.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn2.HeaderText = "id";
            this.idDataGridViewTextBoxColumn2.Name = "idDataGridViewTextBoxColumn2";
            this.idDataGridViewTextBoxColumn2.ReadOnly = true;
            this.idDataGridViewTextBoxColumn2.Visible = false;
            // 
            // nomDataGridViewTextBoxColumn2
            // 
            this.nomDataGridViewTextBoxColumn2.DataPropertyName = "nom";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.nomDataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle6;
            this.nomDataGridViewTextBoxColumn2.HeaderText = "nom";
            this.nomDataGridViewTextBoxColumn2.Name = "nomDataGridViewTextBoxColumn2";
            this.nomDataGridViewTextBoxColumn2.ReadOnly = true;
            this.nomDataGridViewTextBoxColumn2.Width = 150;
            // 
            // nomUsuariDataGridViewTextBoxColumn
            // 
            this.nomUsuariDataGridViewTextBoxColumn.DataPropertyName = "nomUsuari";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.nomUsuariDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this.nomUsuariDataGridViewTextBoxColumn.HeaderText = "nomUsuari";
            this.nomUsuariDataGridViewTextBoxColumn.Name = "nomUsuariDataGridViewTextBoxColumn";
            this.nomUsuariDataGridViewTextBoxColumn.ReadOnly = true;
            this.nomUsuariDataGridViewTextBoxColumn.Width = 150;
            // 
            // correuDataGridViewTextBoxColumn
            // 
            this.correuDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.correuDataGridViewTextBoxColumn.DataPropertyName = "correu";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.correuDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this.correuDataGridViewTextBoxColumn.HeaderText = "correu";
            this.correuDataGridViewTextBoxColumn.Name = "correuDataGridViewTextBoxColumn";
            this.correuDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // contrasenyaDataGridViewTextBoxColumn
            // 
            this.contrasenyaDataGridViewTextBoxColumn.DataPropertyName = "contrasenya";
            this.contrasenyaDataGridViewTextBoxColumn.HeaderText = "contrasenya";
            this.contrasenyaDataGridViewTextBoxColumn.Name = "contrasenyaDataGridViewTextBoxColumn";
            this.contrasenyaDataGridViewTextBoxColumn.ReadOnly = true;
            this.contrasenyaDataGridViewTextBoxColumn.Visible = false;
            // 
            // activitatsconcedidesDataGridViewTextBoxColumn
            // 
            this.activitatsconcedidesDataGridViewTextBoxColumn.DataPropertyName = "activitats_concedides";
            this.activitatsconcedidesDataGridViewTextBoxColumn.HeaderText = "activitats_concedides";
            this.activitatsconcedidesDataGridViewTextBoxColumn.Name = "activitatsconcedidesDataGridViewTextBoxColumn";
            this.activitatsconcedidesDataGridViewTextBoxColumn.ReadOnly = true;
            this.activitatsconcedidesDataGridViewTextBoxColumn.Visible = false;
            // 
            // bindingSourceUsuaris
            // 
            this.bindingSourceUsuaris.DataSource = typeof(usuaris);
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            this.idDataGridViewTextBoxColumn1.Visible = false;
            // 
            // nomDataGridViewTextBoxColumn1
            // 
            this.nomDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nomDataGridViewTextBoxColumn1.DataPropertyName = "nom";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.nomDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle4;
            this.nomDataGridViewTextBoxColumn1.HeaderText = "nom";
            this.nomDataGridViewTextBoxColumn1.Name = "nomDataGridViewTextBoxColumn1";
            this.nomDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // equipsDataGridViewTextBoxColumn1
            // 
            this.equipsDataGridViewTextBoxColumn1.DataPropertyName = "equips";
            this.equipsDataGridViewTextBoxColumn1.HeaderText = "equips";
            this.equipsDataGridViewTextBoxColumn1.Name = "equipsDataGridViewTextBoxColumn1";
            this.equipsDataGridViewTextBoxColumn1.ReadOnly = true;
            this.equipsDataGridViewTextBoxColumn1.Visible = false;
            // 
            // bindingSourceCategories
            // 
            this.bindingSourceCategories.DataSource = typeof(categories);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // nomDataGridViewTextBoxColumn
            // 
            this.nomDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nomDataGridViewTextBoxColumn.DataPropertyName = "nom";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.nomDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.nomDataGridViewTextBoxColumn.HeaderText = "Nom";
            this.nomDataGridViewTextBoxColumn.Name = "nomDataGridViewTextBoxColumn";
            this.nomDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // equipsDataGridViewTextBoxColumn
            // 
            this.equipsDataGridViewTextBoxColumn.DataPropertyName = "equips";
            this.equipsDataGridViewTextBoxColumn.HeaderText = "equips";
            this.equipsDataGridViewTextBoxColumn.Name = "equipsDataGridViewTextBoxColumn";
            this.equipsDataGridViewTextBoxColumn.ReadOnly = true;
            this.equipsDataGridViewTextBoxColumn.Visible = false;
            // 
            // bindingSourceEsports
            // 
            this.bindingSourceEsports.DataSource = typeof(esports);
            // 
            // bindingSourceSexes
            // 
            this.bindingSourceSexes.DataSource = typeof(sexes);
            // 
            // ConfigControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textBoxActualPassword);
            this.Controls.Add(this.labelActualPassword);
            this.Controls.Add(this.textBoxConfirmNewPassword);
            this.Controls.Add(this.labelConfirmNewPassword);
            this.Controls.Add(this.textBoxNewPassword);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.labelNewPassword);
            this.Controls.Add(this.buttonCanviarPassword);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxTemporada);
            this.Controls.Add(this.labelTemporada);
            this.Controls.Add(this.buttonNovaTemporada);
            this.Controls.Add(this.buttonEliminarCategoria);
            this.Controls.Add(this.buttonEditCategoria);
            this.Controls.Add(this.buttonAddCategoria);
            this.Controls.Add(this.buttonEliminarUsuari);
            this.Controls.Add(this.buttonEditUsuari);
            this.Controls.Add(this.buttonAddUsuari);
            this.Controls.Add(this.buttonEliminarEsport);
            this.Controls.Add(this.buttonEditEsport);
            this.Controls.Add(this.buttonAddEsport);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewUsuaris);
            this.Controls.Add(this.dataGridViewCategories);
            this.Controls.Add(this.dataGridViewEsports);
            this.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ConfigControl";
            this.Size = new System.Drawing.Size(1117, 737);
            this.Load += new System.EventHandler(this.ConfigControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEsports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCategories)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUsuaris)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceUsuaris)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCategories)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEsports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceSexes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewEsports;
        private System.Windows.Forms.DataGridView dataGridViewCategories;
        private System.Windows.Forms.DataGridView dataGridViewUsuaris;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonEliminarEsport;
        private System.Windows.Forms.Button buttonEditEsport;
        private System.Windows.Forms.Button buttonAddEsport;
        private System.Windows.Forms.Button buttonEliminarUsuari;
        private System.Windows.Forms.Button buttonEditUsuari;
        private System.Windows.Forms.Button buttonAddUsuari;
        private System.Windows.Forms.Button buttonEliminarCategoria;
        private System.Windows.Forms.Button buttonEditCategoria;
        private System.Windows.Forms.Button buttonAddCategoria;
        private System.Windows.Forms.BindingSource bindingSourceEsports;
        private System.Windows.Forms.BindingSource bindingSourceCategories;
        private System.Windows.Forms.BindingSource bindingSourceSexes;
        private System.Windows.Forms.BindingSource bindingSourceUsuaris;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn equipsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn equipsDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomUsuariDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn correuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contrasenyaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn activitatsconcedidesDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button buttonNovaTemporada;
        private System.Windows.Forms.Label labelTemporada;
        private System.Windows.Forms.TextBox textBoxTemporada;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelNewPassword;
        private System.Windows.Forms.Button buttonCanviarPassword;
        private System.Windows.Forms.TextBox textBoxNewPassword;
        private System.Windows.Forms.Label labelConfirmNewPassword;
        private System.Windows.Forms.TextBox textBoxConfirmNewPassword;
        private System.Windows.Forms.Label labelActualPassword;
        private System.Windows.Forms.TextBox textBoxActualPassword;
    }
}
