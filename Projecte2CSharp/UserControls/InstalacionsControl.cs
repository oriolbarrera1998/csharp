﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projecte2CSharp.DB;
using Projecte2CSharp.Forms;

namespace Projecte2CSharp.UserControls
{
    public partial class InstalacionsControl : UserControl
    {
        public InstalacionsControl()
        {
            InitializeComponent();
        }

        private void InstalacionsControl_Load(object sender, EventArgs e)
        {
            
            getInstalacions();
        }

        public void getInstalacions()
        {
            string msg = "";
            bindingSourceInstalacions.DataSource = ORMInstalacions.selectInstalacions(ref msg).ToList();
            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBoxBusqueda_TextChanged(object sender, EventArgs e)
        {
            string msg = "";
            bindingSourceInstalacions.DataSource = ORMInstalacions.selectInstalacionsByNom(textBoxBusqueda.Text, ref msg).ToList();
            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void InstalacionsControl_Enter(object sender, EventArgs e)
        {
            //dataGridViewInstalacions.Rows[0].Selected = true;
        }

        private void buttonNovaInstalacio_Click(object sender, EventArgs e)
        {
            NewInstalacioForm nis = new NewInstalacioForm();
            nis.ShowDialog();
        }

        private void dataGridViewInstalacions_DoubleClick(object sender, EventArgs e)
        {
            NewInstalacioForm nis = new NewInstalacioForm((instalacions)dataGridViewInstalacions.CurrentRow.DataBoundItem);
            nis.ShowDialog();
        }

        private void buttonModificar_Click(object sender, EventArgs e)
        {
            NewInstalacioForm nis = new NewInstalacioForm((instalacions)dataGridViewInstalacions.CurrentRow.DataBoundItem);
            nis.ShowDialog();
        }

        private void buttonEliminar_Click(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Segur que vols eliminar " + ((instalacions)dataGridViewInstalacions.CurrentRow.DataBoundItem).nom + "?", "Eliminar instal·lació", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r == DialogResult.Yes)
            {
                string msg = "";
                ORMInstalacions.deleteInstalacio((instalacions)dataGridViewInstalacions.CurrentRow.DataBoundItem);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                getInstalacions();
            }
        }

        private void dataGridViewInstalacions_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            DialogResult r = MessageBox.Show("Segur que vols eliminar " + ((instalacions)dataGridViewInstalacions.CurrentRow.DataBoundItem).nom + "?", "Eliminar instal·lació", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r == DialogResult.Yes)
            {
                string msg = "";
                ORMInstalacions.deleteInstalacio((instalacions)dataGridViewInstalacions.CurrentRow.DataBoundItem);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = true;
            }
        }
    }

    
}
