﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projecte2CSharp.DB;
using Projecte2CSharp.Forms;
using System.Text.RegularExpressions;
using Projecte2CSharp.Clases;

namespace Projecte2CSharp.UserControls
{
    public partial class ConfigControl : UserControl
    {
        public usuaris user { get; set; }

        public ConfigControl()
        {
            InitializeComponent();
        }

        private void ConfigControl_Load(object sender, EventArgs e)
        {
            getData();
            buttonNovaTemporada.Enabled = false;
        }

        public void getData()
        {
            string msg = "";
            bindingSourceCategories.DataSource = ORMCategories.selectCategories(ref msg).ToList();

            bindingSourceEsports.DataSource = ORMEsports.selectEsports(ref msg).ToList();
            bindingSourceSexes.DataSource = ORMSexes.selectSexes(ref msg).ToList();
            bindingSourceUsuaris.DataSource = ORMUsuaris.selectUsuariExceptCurrent(user.id, ref msg);

            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        
        

        private void dataGridViewEsports_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            DialogResult r = MessageBox.Show("Segur que vols eliminar " + ((esports)dataGridViewEsports.CurrentRow.DataBoundItem).nom + "?", "Eliminar esport", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            string msg = "";
            if (r == DialogResult.Yes)
            {
                ORMEsports.deleteEsport((esports)dataGridViewEsports.CurrentRow.DataBoundItem);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        

        private void textBoxTemporada_TextChanged(object sender, EventArgs e)
        {
            if (textBoxTemporada.Text.Equals(string.Empty))
            {
                buttonNovaTemporada.Enabled = false;
                
            }
            else {
                buttonNovaTemporada.Enabled = true;
            }
        }

        private void buttonNovaTemporada_Click(object sender, EventArgs e)
        {
            
            //------------ ARREGLAR REGEX ------------------------
            var regex = @"\d{4}-\d{4}";
            Match match = Regex.Match(textBoxTemporada.Text, regex);

            if (match.Success)
            {
                MessageBox.Show("La nova temporada s'ha creat correctament.", "Nova temporada creada", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("NO", "Nova temporada creada", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            //Actualitza dades de la BD

            // ----------- COMENTAT PER PRECAUCIO -----------------
            //temporades temporada = new temporades();
            //temporada.temporada = textBoxTemporada.Text;
            //ORMEntitats.generarTemporada(temporada);
            //MessageBox.Show("La nova temporada s'ha creat correctament.", "Nova temporada creada", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void textBoxTemporada_Click(object sender, EventArgs e)
        {
            textBoxTemporada.Clear();
        }

        private void buttonAddEsport_Click(object sender, EventArgs e)
        {
            NewEsportForm nef = new NewEsportForm();
            nef.ShowDialog();
        }

        private void buttonEditEsport_Click(object sender, EventArgs e)
        {
            NewEsportForm nef = new NewEsportForm((esports)dataGridViewEsports.CurrentRow.DataBoundItem);
            nef.ShowDialog();
        }

        private void dataGridViewEsports_DoubleClick(object sender, EventArgs e)
        {
            NewEsportForm nef = new NewEsportForm((esports)dataGridViewEsports.CurrentRow.DataBoundItem);
            nef.ShowDialog();
        }

        private void buttonEliminarEsport_Click(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Segur que vols eliminar " + ((esports)dataGridViewEsports.CurrentRow.DataBoundItem).nom + "?", "Eliminar esport", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r == DialogResult.Yes)
            {
                string msg = "";
                ORMEsports.deleteEsport((esports)dataGridViewEsports.CurrentRow.DataBoundItem);
                bindingSourceEsports.DataSource = ORMEsports.selectEsports(ref msg).ToList();

                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
        }

        private void buttonAddCategoria_Click(object sender, EventArgs e)
        {
            NewCategoriaForm ncf = new NewCategoriaForm();
            ncf.ShowDialog();
        }

        private void buttonEditCategoria_Click(object sender, EventArgs e)
        {
            NewCategoriaForm ncf = new NewCategoriaForm((categories)dataGridViewCategories.CurrentRow.DataBoundItem);
            ncf.ShowDialog();
        }

        private void dataGridViewCategories_DoubleClick(object sender, EventArgs e)
        {
            NewCategoriaForm ncf = new NewCategoriaForm((categories)dataGridViewCategories.CurrentRow.DataBoundItem);
            ncf.ShowDialog();
        }

        private void buttonEliminarCategoria_Click(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Segur que vols eliminar " + ((categories)dataGridViewCategories.CurrentRow.DataBoundItem).nom + "?", "Eliminar categoria", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r == DialogResult.Yes)
            {
                string msg = "";
                ORMCategories.deleteCategoria((categories)dataGridViewCategories.CurrentRow.DataBoundItem);
                bindingSourceCategories.DataSource = ORMCategories.selectCategories(ref msg).ToList();
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void dataGridViewCategories_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            DialogResult r = MessageBox.Show("Segur que vols eliminar " + ((categories)dataGridViewCategories.CurrentRow.DataBoundItem).nom + "?", "Eliminar categoria", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r == DialogResult.Yes)
            {
                string msg = "";
                ORMCategories.deleteCategoria((categories)dataGridViewCategories.CurrentRow.DataBoundItem);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        public void enablePasswordChange()
        {
            foreach(Control c in this.Controls)
            {
                c.Enabled = false;
            }

            labelNewPassword.Enabled = true;
            labelActualPassword.Enabled = true;
            labelConfirmNewPassword.Enabled = true;
            textBoxActualPassword.Enabled = true;
            textBoxNewPassword.Enabled = true;
            textBoxConfirmNewPassword.Enabled = true;
            buttonCanviarPassword.Enabled = true;
        }

        private void buttonCanviarPassword_Click(object sender, EventArgs e)
        {
            string msg = "";
            if (user.contrasenya.Equals(Hash.hashString(textBoxActualPassword.Text)))
            {
                if (textBoxNewPassword.Text.Equals(textBoxConfirmNewPassword.Text))
                {
                    user.contrasenya = Hash.hashString(textBoxNewPassword.Text);
                    msg = ORMUsuaris.updateUsuari(user);
                    if (!msg.Equals(""))
                    {
                        MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("La contrasenya s'ha guardat correctament", "Exit", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    
                    
                    textBoxNewPassword.Clear();
                    textBoxConfirmNewPassword.Clear();
                    textBoxActualPassword.Clear();
                }
            
                else
                {
                    MessageBox.Show("Les contrasenyes no coincideixen", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Contrasenya incorrecte", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonAddUsuari_Click(object sender, EventArgs e)
        {
            NewUsuariForm nuf = new NewUsuariForm();
            nuf.ShowDialog();
        }

        private void buttonEditUsuari_Click(object sender, EventArgs e)
        {
            NewUsuariForm nuf = new NewUsuariForm((usuaris)dataGridViewUsuaris.CurrentRow.DataBoundItem);
            nuf.ShowDialog();
        }

        private void dataGridViewUsuaris_DoubleClick(object sender, EventArgs e)
        {
            NewUsuariForm nuf = new NewUsuariForm((usuaris)dataGridViewUsuaris.CurrentRow.DataBoundItem);
            nuf.ShowDialog();
        }

        private void buttonEliminarUsuari_Click(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Segur que vols eliminar " + ((usuaris)dataGridViewUsuaris.CurrentRow.DataBoundItem).nom + "?", "Eliminar usuari", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r == DialogResult.Yes)
            {
                string msg = "";
                ORMUsuaris.deleteUsuari((usuaris)dataGridViewUsuaris.CurrentRow.DataBoundItem);
               
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    bindingSourceUsuaris.DataSource = ORMUsuaris.selectUsuariExceptCurrent(user.id, ref msg);
                }
            }
            
        }

        private void dataGridViewUsuaris_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            DialogResult r = MessageBox.Show("Segur que vols eliminar " + ((usuaris)dataGridViewUsuaris.CurrentRow.DataBoundItem).nom + "?", "Eliminar usuari", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r == DialogResult.Yes)
            {
                string msg = "";
                ORMUsuaris.deleteUsuari((usuaris)dataGridViewUsuaris.CurrentRow.DataBoundItem);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }

            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}
