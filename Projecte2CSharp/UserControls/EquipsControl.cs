﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projecte2CSharp.DB;
using Projecte2CSharp.Forms;

namespace Projecte2CSharp.UserControls
{
    public partial class EquipsControl : UserControl
    {
        public EquipsControl()
        {
            InitializeComponent();
        }

        private void EquipsControl_Load(object sender, EventArgs e)
        {
            getEquips();
        }

        public void getEquips()
        {
            string msg = "";
            bindingSourceEquips.DataSource = ORMEquips.selectEquips(ref msg);
            bindingSourceEntitats.DataSource = ORMEntitats.selectEntitats(ref msg);
            bindingSourceSexes.DataSource = ORMSexes.selectSexes(ref msg);
            bindingSourceEsport.DataSource = ORMEsports.selectEsports(ref msg);
            bindingSourceCategories.DataSource = ORMCategories.selectCategories(ref msg);
            bindingSourceCompeticions.DataSource = ORMCompeticio.selectCompeticions(ref msg);

            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        

        private void dataGridViewInstalacions_DoubleClick(object sender, EventArgs e)
        {
            EquipForm ef = new EquipForm((equips)dataGridViewEquips.CurrentRow.DataBoundItem);
            ef.ShowDialog();
        }

        private void buttonNou_Click(object sender, EventArgs e)
        {
            EquipForm ef = new EquipForm();
            ef.ShowDialog();
        }

        private void buttonModificar_Click(object sender, EventArgs e)
        {
            EquipForm ef = new EquipForm((equips)dataGridViewEquips.CurrentRow.DataBoundItem);
            ef.ShowDialog();
        }

        private void buttonEliminar_Click(object sender, EventArgs e)
        {
            equips equip = (equips)dataGridViewEquips.CurrentRow.DataBoundItem;
            DialogResult result = MessageBox.Show("Segur que vols eliminar el equip: " + equip.nom + "?", "Eliminar equip", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                string msg = ORMEquips.deleteEquip(equip);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                getEquips();
            }
        }

        private void dataGridViewEquips_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            equips equip = (equips)dataGridViewEquips.CurrentRow.DataBoundItem;
            DialogResult result = MessageBox.Show("Segur que vols eliminar el equip: " + equip.nom + "?", "Eliminar equip", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                string msg = ORMEquips.deleteEquip(equip);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void EquipsControl_Enter(object sender, EventArgs e)
        {
            if(dataGridViewEquips.Rows.Count != 0){
                dataGridViewEquips.Rows[0].Selected = true;
            }
            
        }

        private void textBoxBusqueda_TextChanged(object sender, EventArgs e)
        {
            string msg = "";
            bindingSourceEquips.DataSource = ORMEquips.selectEquipsByNom(textBoxBusqueda.Text, ref msg);
            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
