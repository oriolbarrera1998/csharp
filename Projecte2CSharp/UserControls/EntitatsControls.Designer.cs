﻿namespace Projecte2CSharp.UserControls
{
    partial class EntitatsControls
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.bindingSourceEntitats = new System.Windows.Forms.BindingSource(this.components);
            this.textBoxBusqueda = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonEliminar = new System.Windows.Forms.Button();
            this.buttonModificar = new System.Windows.Forms.Button();
            this.buttonNovaEntitat = new System.Windows.Forms.Button();
            this.radioButtonBuscarPerCIF = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButtonBuscarPerNom = new System.Windows.Forms.RadioButton();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.nomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.direccioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cifDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.correuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEntitats)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeight = 35;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nomDataGridViewTextBoxColumn,
            this.Column1,
            this.direccioDataGridViewTextBoxColumn,
            this.cifDataGridViewTextBoxColumn,
            this.correuDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.bindingSourceEntitats;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.dataGridView1.Location = new System.Drawing.Point(27, 212);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.RowTemplate.Height = 35;
            this.dataGridView1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(933, 486);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridView1_UserDeletingRow);
            this.dataGridView1.DoubleClick += new System.EventHandler(this.dataGridView1_DoubleClick);
            // 
            // bindingSourceEntitats
            // 
            this.bindingSourceEntitats.DataSource = typeof(Projecte2CSharp.entitats);
            // 
            // textBoxBusqueda
            // 
            this.textBoxBusqueda.Location = new System.Drawing.Point(27, 33);
            this.textBoxBusqueda.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxBusqueda.Name = "textBoxBusqueda";
            this.textBoxBusqueda.Size = new System.Drawing.Size(274, 28);
            this.textBoxBusqueda.TabIndex = 3;
            this.textBoxBusqueda.TextChanged += new System.EventHandler(this.textBoxBusqueda_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "Buscar";
            // 
            // buttonEliminar
            // 
            this.buttonEliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(181)))));
            this.buttonEliminar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonEliminar.FlatAppearance.BorderSize = 3;
            this.buttonEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEliminar.Image = global::Projecte2CSharp.Properties.Resources.trash;
            this.buttonEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonEliminar.Location = new System.Drawing.Point(843, 132);
            this.buttonEliminar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.buttonEliminar.Name = "buttonEliminar";
            this.buttonEliminar.Size = new System.Drawing.Size(116, 42);
            this.buttonEliminar.TabIndex = 10;
            this.buttonEliminar.Text = "Borrar";
            this.buttonEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonEliminar.UseVisualStyleBackColor = false;
            this.buttonEliminar.Click += new System.EventHandler(this.buttonEliminar_Click);
            // 
            // buttonModificar
            // 
            this.buttonModificar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(182)))));
            this.buttonModificar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonModificar.FlatAppearance.BorderSize = 3;
            this.buttonModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonModificar.Image = global::Projecte2CSharp.Properties.Resources.edit;
            this.buttonModificar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonModificar.Location = new System.Drawing.Point(723, 132);
            this.buttonModificar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.buttonModificar.Name = "buttonModificar";
            this.buttonModificar.Size = new System.Drawing.Size(116, 42);
            this.buttonModificar.TabIndex = 9;
            this.buttonModificar.Text = "Editar";
            this.buttonModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonModificar.UseVisualStyleBackColor = false;
            this.buttonModificar.Click += new System.EventHandler(this.buttonModificar_Click);
            // 
            // buttonNovaEntitat
            // 
            this.buttonNovaEntitat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonNovaEntitat.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonNovaEntitat.FlatAppearance.BorderSize = 3;
            this.buttonNovaEntitat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNovaEntitat.Image = global::Projecte2CSharp.Properties.Resources.add;
            this.buttonNovaEntitat.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonNovaEntitat.Location = new System.Drawing.Point(27, 132);
            this.buttonNovaEntitat.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.buttonNovaEntitat.Name = "buttonNovaEntitat";
            this.buttonNovaEntitat.Size = new System.Drawing.Size(274, 42);
            this.buttonNovaEntitat.TabIndex = 2;
            this.buttonNovaEntitat.Text = "     Nova Entitat";
            this.buttonNovaEntitat.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonNovaEntitat.UseVisualStyleBackColor = false;
            this.buttonNovaEntitat.Click += new System.EventHandler(this.buttonNovaEntitat_Click);
            // 
            // radioButtonBuscarPerCIF
            // 
            this.radioButtonBuscarPerCIF.AutoSize = true;
            this.radioButtonBuscarPerCIF.BackColor = System.Drawing.Color.Transparent;
            this.radioButtonBuscarPerCIF.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioButtonBuscarPerCIF.Location = new System.Drawing.Point(145, 78);
            this.radioButtonBuscarPerCIF.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.radioButtonBuscarPerCIF.Name = "radioButtonBuscarPerCIF";
            this.radioButtonBuscarPerCIF.Size = new System.Drawing.Size(54, 25);
            this.radioButtonBuscarPerCIF.TabIndex = 6;
            this.radioButtonBuscarPerCIF.TabStop = true;
            this.radioButtonBuscarPerCIF.Text = "CIF";
            this.radioButtonBuscarPerCIF.UseVisualStyleBackColor = false;
            this.radioButtonBuscarPerCIF.CheckedChanged += new System.EventHandler(this.radioButtonBuscarPerCIF_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Filtrar per:";
            // 
            // radioButtonBuscarPerNom
            // 
            this.radioButtonBuscarPerNom.AutoSize = true;
            this.radioButtonBuscarPerNom.Location = new System.Drawing.Point(215, 78);
            this.radioButtonBuscarPerNom.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.radioButtonBuscarPerNom.Name = "radioButtonBuscarPerNom";
            this.radioButtonBuscarPerNom.Size = new System.Drawing.Size(59, 24);
            this.radioButtonBuscarPerNom.TabIndex = 5;
            this.radioButtonBuscarPerNom.TabStop = true;
            this.radioButtonBuscarPerNom.Text = "Nom";
            this.radioButtonBuscarPerNom.UseVisualStyleBackColor = true;
            this.radioButtonBuscarPerNom.CheckedChanged += new System.EventHandler(this.radioButtonBuscarPerNom_CheckedChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "temporades";
            this.dataGridViewTextBoxColumn1.HeaderText = "temporades";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.button1.FlatAppearance.BorderSize = 3;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(723, 84);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(237, 42);
            this.button1.TabIndex = 11;
            this.button1.Text = "Veure demandes";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // nomDataGridViewTextBoxColumn
            // 
            this.nomDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nomDataGridViewTextBoxColumn.DataPropertyName = "nom";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.nomDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.nomDataGridViewTextBoxColumn.HeaderText = "Nom";
            this.nomDataGridViewTextBoxColumn.Name = "nomDataGridViewTextBoxColumn";
            this.nomDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "id";
            this.Column1.HeaderText = "id";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // direccioDataGridViewTextBoxColumn
            // 
            this.direccioDataGridViewTextBoxColumn.DataPropertyName = "direccio";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.direccioDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.direccioDataGridViewTextBoxColumn.HeaderText = "Adreça";
            this.direccioDataGridViewTextBoxColumn.Name = "direccioDataGridViewTextBoxColumn";
            this.direccioDataGridViewTextBoxColumn.ReadOnly = true;
            this.direccioDataGridViewTextBoxColumn.Width = 200;
            // 
            // cifDataGridViewTextBoxColumn
            // 
            this.cifDataGridViewTextBoxColumn.DataPropertyName = "cif";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.cifDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.cifDataGridViewTextBoxColumn.HeaderText = "CIF";
            this.cifDataGridViewTextBoxColumn.Name = "cifDataGridViewTextBoxColumn";
            this.cifDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // correuDataGridViewTextBoxColumn
            // 
            this.correuDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.correuDataGridViewTextBoxColumn.DataPropertyName = "correu";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.correuDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.correuDataGridViewTextBoxColumn.HeaderText = "Correu";
            this.correuDataGridViewTextBoxColumn.Name = "correuDataGridViewTextBoxColumn";
            this.correuDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // EntitatsControls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonEliminar);
            this.Controls.Add(this.buttonModificar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.radioButtonBuscarPerCIF);
            this.Controls.Add(this.radioButtonBuscarPerNom);
            this.Controls.Add(this.textBoxBusqueda);
            this.Controls.Add(this.buttonNovaEntitat);
            this.Controls.Add(this.dataGridView1);
            this.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "EntitatsControls";
            this.Size = new System.Drawing.Size(993, 730);
            this.Load += new System.EventHandler(this.EntitatsControls_Load);
            this.Enter += new System.EventHandler(this.EntitatsControls_Enter);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEntitats)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonNovaEntitat;
        private System.Windows.Forms.TextBox textBoxBusqueda;
        private System.Windows.Forms.BindingSource bindingSourceEntitats;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonModificar;
        private System.Windows.Forms.Button buttonEliminar;
        private System.Windows.Forms.RadioButton radioButtonBuscarPerCIF;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButtonBuscarPerNom;
        private System.Windows.Forms.DataGridViewTextBoxColumn temporadaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn direccioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cifDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn correuDataGridViewTextBoxColumn;
    }
}
