﻿namespace Projecte2CSharp.UserControls
{
    partial class InstalacionsControl
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxBusqueda = new System.Windows.Forms.TextBox();
            this.dataGridViewInstalacions = new System.Windows.Forms.DataGridView();
            this.bindingSourceInstalacions = new System.Windows.Forms.BindingSource(this.components);
            this.buttonEliminar = new System.Windows.Forms.Button();
            this.buttonModificar = new System.Windows.Forms.Button();
            this.buttonNovaInstalacio = new System.Windows.Forms.Button();
            this.categoriesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gestioexternaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.direccioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInstalacions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceInstalacions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoriesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 20);
            this.label2.TabIndex = 17;
            this.label2.Text = "Buscar";
            // 
            // textBoxBusqueda
            // 
            this.textBoxBusqueda.Location = new System.Drawing.Point(27, 33);
            this.textBoxBusqueda.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxBusqueda.Name = "textBoxBusqueda";
            this.textBoxBusqueda.Size = new System.Drawing.Size(274, 28);
            this.textBoxBusqueda.TabIndex = 13;
            this.textBoxBusqueda.TextChanged += new System.EventHandler(this.textBoxBusqueda_TextChanged);
            // 
            // dataGridViewInstalacions
            // 
            this.dataGridViewInstalacions.AllowUserToAddRows = false;
            this.dataGridViewInstalacions.AllowUserToResizeColumns = false;
            this.dataGridViewInstalacions.AllowUserToResizeRows = false;
            this.dataGridViewInstalacions.AutoGenerateColumns = false;
            this.dataGridViewInstalacions.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewInstalacions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewInstalacions.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridViewInstalacions.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewInstalacions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewInstalacions.ColumnHeadersHeight = 35;
            this.dataGridViewInstalacions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewInstalacions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nomDataGridViewTextBoxColumn,
            this.gestioexternaDataGridViewTextBoxColumn,
            this.direccioDataGridViewTextBoxColumn});
            this.dataGridViewInstalacions.DataSource = this.bindingSourceInstalacions;
            this.dataGridViewInstalacions.EnableHeadersVisualStyles = false;
            this.dataGridViewInstalacions.Location = new System.Drawing.Point(27, 167);
            this.dataGridViewInstalacions.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dataGridViewInstalacions.MultiSelect = false;
            this.dataGridViewInstalacions.Name = "dataGridViewInstalacions";
            this.dataGridViewInstalacions.ReadOnly = true;
            this.dataGridViewInstalacions.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewInstalacions.RowHeadersVisible = false;
            this.dataGridViewInstalacions.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewInstalacions.RowTemplate.Height = 35;
            this.dataGridViewInstalacions.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewInstalacions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewInstalacions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewInstalacions.Size = new System.Drawing.Size(933, 533);
            this.dataGridViewInstalacions.TabIndex = 11;
            this.dataGridViewInstalacions.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridViewInstalacions_UserDeletingRow);
            this.dataGridViewInstalacions.DoubleClick += new System.EventHandler(this.dataGridViewInstalacions_DoubleClick);
            // 
            // bindingSourceInstalacions
            // 
            this.bindingSourceInstalacions.DataSource = typeof(Projecte2CSharp.instalacions);
            // 
            // buttonEliminar
            // 
            this.buttonEliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(181)))));
            this.buttonEliminar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonEliminar.FlatAppearance.BorderSize = 3;
            this.buttonEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEliminar.Image = global::Projecte2CSharp.Properties.Resources.trash;
            this.buttonEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonEliminar.Location = new System.Drawing.Point(844, 86);
            this.buttonEliminar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.buttonEliminar.Name = "buttonEliminar";
            this.buttonEliminar.Size = new System.Drawing.Size(116, 42);
            this.buttonEliminar.TabIndex = 22;
            this.buttonEliminar.Text = "Borrar";
            this.buttonEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonEliminar.UseVisualStyleBackColor = false;
            this.buttonEliminar.Click += new System.EventHandler(this.buttonEliminar_Click);
            // 
            // buttonModificar
            // 
            this.buttonModificar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(182)))));
            this.buttonModificar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonModificar.FlatAppearance.BorderSize = 3;
            this.buttonModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonModificar.Image = global::Projecte2CSharp.Properties.Resources.edit;
            this.buttonModificar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonModificar.Location = new System.Drawing.Point(724, 86);
            this.buttonModificar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.buttonModificar.Name = "buttonModificar";
            this.buttonModificar.Size = new System.Drawing.Size(116, 42);
            this.buttonModificar.TabIndex = 21;
            this.buttonModificar.Text = "Editar";
            this.buttonModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonModificar.UseVisualStyleBackColor = false;
            this.buttonModificar.Click += new System.EventHandler(this.buttonModificar_Click);
            // 
            // buttonNovaInstalacio
            // 
            this.buttonNovaInstalacio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonNovaInstalacio.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonNovaInstalacio.FlatAppearance.BorderSize = 3;
            this.buttonNovaInstalacio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNovaInstalacio.Image = global::Projecte2CSharp.Properties.Resources.add;
            this.buttonNovaInstalacio.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonNovaInstalacio.Location = new System.Drawing.Point(27, 86);
            this.buttonNovaInstalacio.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.buttonNovaInstalacio.Name = "buttonNovaInstalacio";
            this.buttonNovaInstalacio.Size = new System.Drawing.Size(274, 42);
            this.buttonNovaInstalacio.TabIndex = 20;
            this.buttonNovaInstalacio.Text = "     Nova Instal·lació";
            this.buttonNovaInstalacio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonNovaInstalacio.UseVisualStyleBackColor = false;
            this.buttonNovaInstalacio.Click += new System.EventHandler(this.buttonNovaInstalacio_Click);
            // 
            // categoriesBindingSource
            // 
            this.categoriesBindingSource.DataSource = typeof(Projecte2CSharp.categories);
            // 
            // nomDataGridViewTextBoxColumn
            // 
            this.nomDataGridViewTextBoxColumn.DataPropertyName = "nom";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.nomDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.nomDataGridViewTextBoxColumn.HeaderText = "Nom";
            this.nomDataGridViewTextBoxColumn.Name = "nomDataGridViewTextBoxColumn";
            this.nomDataGridViewTextBoxColumn.ReadOnly = true;
            this.nomDataGridViewTextBoxColumn.Width = 300;
            // 
            // gestioexternaDataGridViewTextBoxColumn
            // 
            this.gestioexternaDataGridViewTextBoxColumn.DataPropertyName = "externa_text";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.gestioexternaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.gestioexternaDataGridViewTextBoxColumn.HeaderText = "Extern";
            this.gestioexternaDataGridViewTextBoxColumn.Name = "gestioexternaDataGridViewTextBoxColumn";
            this.gestioexternaDataGridViewTextBoxColumn.ReadOnly = true;
            this.gestioexternaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // direccioDataGridViewTextBoxColumn
            // 
            this.direccioDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.direccioDataGridViewTextBoxColumn.DataPropertyName = "direccio";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.direccioDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.direccioDataGridViewTextBoxColumn.HeaderText = "Adreça";
            this.direccioDataGridViewTextBoxColumn.Name = "direccioDataGridViewTextBoxColumn";
            this.direccioDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // InstalacionsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonEliminar);
            this.Controls.Add(this.buttonModificar);
            this.Controls.Add(this.buttonNovaInstalacio);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxBusqueda);
            this.Controls.Add(this.dataGridViewInstalacions);
            this.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "InstalacionsControl";
            this.Size = new System.Drawing.Size(993, 730);
            this.Load += new System.EventHandler(this.InstalacionsControl_Load);
            this.Enter += new System.EventHandler(this.InstalacionsControl_Enter);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInstalacions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceInstalacions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoriesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxBusqueda;
        private System.Windows.Forms.DataGridView dataGridViewInstalacions;
        private System.Windows.Forms.BindingSource bindingSourceInstalacions;
        private System.Windows.Forms.DataGridViewTextBoxColumn horarisinstalacionsDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button buttonEliminar;
        private System.Windows.Forms.Button buttonModificar;
        private System.Windows.Forms.Button buttonNovaInstalacio;
        private System.Windows.Forms.BindingSource categoriesBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gestioexternaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn direccioDataGridViewTextBoxColumn;
    }
}
