﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projecte2CSharp.DB;
using Projecte2CSharp.Forms;
using Projecte2CSharp.Clases.POJO;

namespace Projecte2CSharp.UserControls
{
    public partial class EntitatsControls : UserControl
    {

        
        public EntitatsControls()
        {
            InitializeComponent();
        }

        private void EntitatsControls_Load(object sender, EventArgs e)
        {
            radioButtonBuscarPerCIF.Checked = true;
            
            getEntitats();
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            EntitatForm eform = new EntitatForm((entitats)dataGridView1.CurrentRow.DataBoundItem);
            eform.ShowDialog();
        }

        private void buttonModificar_Click(object sender, EventArgs e)
        {
            EntitatForm ef = new EntitatForm((entitats)dataGridView1.CurrentRow.DataBoundItem);
            ef.ShowDialog();
        }

        private void buttonNovaEntitat_Click(object sender, EventArgs e)
        {
            EntitatForm ef = new EntitatForm();
            ef.ShowDialog();
        }

        public void getEntitats()
        {
            string msg = "";
            bindingSourceEntitats.DataSource = ORMEntitats.selectEntitats(ref msg);
            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            DialogResult r = MessageBox.Show("Segur que vols eliminar " + ((entitats)dataGridView1.CurrentRow.DataBoundItem).nom + "?", "Eliminar entitat", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r == DialogResult.Yes)
            {
                string msg = ORMEntitats.deleteEntitat((entitats)dataGridView1.CurrentRow.DataBoundItem);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void textBoxBusqueda_TextChanged(object sender, EventArgs e)
        {
            string msg = "";
            if (radioButtonBuscarPerCIF.Checked)
            {
                
                bindingSourceEntitats.DataSource = ORMEntitats.selectEntitatByCIF(textBoxBusqueda.Text, ref msg);
                if(!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                bindingSourceEntitats.DataSource = ORMEntitats.selectEntitatByNom(textBoxBusqueda.Text, ref msg);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void radioButtonBuscarPerNom_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonBuscarPerNom.Checked)
            {
                string msg = "";
                bindingSourceEntitats.DataSource = ORMEntitats.selectEntitatByNom(textBoxBusqueda.Text, ref msg);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
        }

        private void radioButtonBuscarPerCIF_CheckedChanged(object sender, EventArgs e)
        {
            string msg = "";
            if (radioButtonBuscarPerCIF.Checked)
            {
                bindingSourceEntitats.DataSource = ORMEntitats.selectEntitatByCIF(textBoxBusqueda.Text, ref msg);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
        }

        

        private void buttonEliminar_Click(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Segur que vols eliminar " + ((entitats)dataGridView1.CurrentRow.DataBoundItem).nom + "?", "Eliminar entitat", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r == DialogResult.Yes)
            {
                
                string msg = ORMEntitats.deleteEntitat((entitats)dataGridView1.CurrentRow.DataBoundItem);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                getEntitats();
            }
            
            
        }

        private void EntitatsControls_Enter(object sender, EventArgs e)
        {
            if(dataGridView1.Rows.Count != 0)
            {
                dataGridView1.Rows[0].Selected = true;
            }
           
        }

        private void button1_Click(object sender, EventArgs e)
        {

            EntitatDemandesForm ed = new EntitatDemandesForm((entitats)dataGridView1.CurrentRow.DataBoundItem);
            ed.Show();

            
            
        }
    }
}
