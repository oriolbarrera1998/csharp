﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projecte2CSharp.Clases.POJO
{
    public class HorariComplet
    {
        public System.TimeSpan hora_inici { get; set; }
        public System.TimeSpan hora_final { get; set; }
        public string nomInstalacio { get; set; }
        public string  nomEspai { get; set; }
        public string tipus { get; set; }
        public string dia { get; set; }

        public HorariComplet(System.TimeSpan hora_inici, System.TimeSpan hora_final, string nomInstalacio, string nomEspai, string tipus, string dia)
        {
            this.hora_inici = hora_inici;
            this.hora_final = hora_final;
            this.nomInstalacio = nomInstalacio;
            this.nomEspai = nomEspai;
            this.tipus = tipus;
            this.dia = dia;
        }
    }
}
