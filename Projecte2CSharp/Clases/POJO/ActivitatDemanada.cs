﻿using Projecte2CSharp.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.Clases.POJO
{
    public class ActivitatDemanada
    {
        public int id { get; set; }
        public string nom { get; set; }
        public string tipus { get; set; }
        public int id_espai { get; set; }
        public int id_equip { get; set; }
        public int durada { get; set; }
        public int dies { get; set; }
        public byte es_assignada { get; set; }
        public string instalacio { get; set; }
        public string entitat { get; set; }
        public int id_instalacio { get; set; }

        public static List<ActivitatDemanada> createDemandes(List<activitats_demanades> activitats_demanades)
        {
            List<ActivitatDemanada> activitats = new List<ActivitatDemanada>();

            foreach(activitats_demanades a in activitats_demanades)
            {
                ActivitatDemanada actD = new ActivitatDemanada();
                actD.id = a.id;
                actD.nom = a.nom;
                actD.tipus = a.tipus;
                actD.id_espai = a.id_espai;
                actD.id_equip = a.id_equip;
                actD.durada = a.durada;
                actD.dies = a.dies;
                actD.es_assignada = a.es_assignada;

                string msg = "";
                espais espai = ORMEspais.getEspaiByID(actD.id_espai, ref msg);
                instalacions instalacio = ORMInstalacions.getInsatalacioByID(espai.id_instalacio, ref msg);
                actD.instalacio = instalacio.nom;
                actD.id_instalacio = instalacio.id;
                equips equip = ORMEquips.selectEquipByID(actD.id_equip, ref msg);
                actD.entitat = ORMEntitats.selectEntitatByID(equip.id_entitat, ref msg).nom;
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                activitats.Add(actD);
            }

            return activitats;
        }
    }
}
