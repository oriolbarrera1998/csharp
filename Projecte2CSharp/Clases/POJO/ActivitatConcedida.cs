﻿using Projecte2CSharp.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.Clases.POJO
{
    public class ActivitatConcedida
    {
        public int id { get; set; }
        public List<horaris_activitats> horaris { get; set; }

        public ActivitatConcedida(int id)
        {
            string msg = "";
            this.id = id;
            horaris = ORMHoraris_activitats.selectHorarisActivitatsByIdActivitat(id, ref msg);
            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                horaris = null;
            }
        }
    }
}
