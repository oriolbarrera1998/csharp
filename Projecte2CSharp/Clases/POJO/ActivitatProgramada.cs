﻿using Projecte2CSharp.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.Clases.POJO
{
    public class ActivitatProgramada
    {
        public int id { get; set; }
        public string tipus { get; set; }
        public int id_espai { get; set; }
        public int id_equip { get; set; }
        public string instalacio { get; set; }
        public int id_activitat_demanada { get; set; }
        public int id_usuari { get; set; }
        public string nom { get; set; }
        public string entitat { get; set; }
        public int id_instalacio { get; set; }

        public static List<ActivitatProgramada> createDemandes(List<activitats_concedides> activitats_concedides)
        {
            List<ActivitatProgramada> activitats = new List<ActivitatProgramada>();

            foreach (activitats_concedides a in activitats_concedides)
            {
                ActivitatProgramada actD = new ActivitatProgramada();
                actD.id = a.id;
                actD.nom = a.nom;
                actD.tipus = a.tipus;
                actD.id_espai = a.id_espai;
                actD.id_equip = a.id_equip;
                

                string msg = "";
                espais espai = ORMEspais.getEspaiByID(actD.id_espai, ref msg);
                instalacions instalacio = ORMInstalacions.getInsatalacioByID(espai.id_instalacio, ref msg);
                actD.instalacio = instalacio.nom;
                actD.id_instalacio = instalacio.id;
                equips equip = ORMEquips.selectEquipByID(actD.id_equip, ref msg);
                actD.entitat = ORMEntitats.selectEntitatByID(equip.id_entitat, ref msg).nom;
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                activitats.Add(actD);
            }

            return activitats;
        }


        public static List<ActivitatProgramada> createDemandesByEquip(List<activitats_concedides> activitats_concedides)
        {
            List<ActivitatProgramada> activitats = new List<ActivitatProgramada>();

            foreach (activitats_concedides a in activitats_concedides)
            {
                ActivitatProgramada actD = new ActivitatProgramada();
                actD.id = a.id;
                actD.nom = a.nom;
                actD.tipus = a.tipus;
                actD.id_espai = a.id_espai;
                actD.id_equip = a.id_equip;


                string msg = "";
                espais espai = ORMEspais.getEspaiByID(actD.id_espai, ref msg);
                instalacions instalacio = ORMInstalacions.getInsatalacioByID(espai.id_instalacio, ref msg);
                actD.instalacio = instalacio.nom;
                actD.id_instalacio = instalacio.id;
                equips equip = ORMEquips.selectEquipByID(actD.id_equip, ref msg);
                actD.entitat = ORMEntitats.selectEntitatByID(equip.id_entitat, ref msg).nom;
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                
                activitats.Add(actD);
                
                
            }

            return activitats;
        }
    }
}
