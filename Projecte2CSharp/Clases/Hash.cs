﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Projecte2CSharp.Clases
{
    public static class Hash
    {
        public static string hashString(String cadena)
        {
            SHA256 sha256 = SHA256.Create();

            byte[] hashed = sha256.ComputeHash(Encoding.UTF8.GetBytes(cadena));
            StringBuilder sb = new StringBuilder();
            
            foreach(byte b in hashed)
            {
                sb.Append(b.ToString("x2"));
            }

            return sb.ToString();
        }
    }
}
