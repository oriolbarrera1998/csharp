﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.Clases.Exceptions
{
    class NullUserException: Exception
    {
        private const String MSG = "Usuari o contrasenya incorrecte";
        public NullUserException() : base(MSG)
        {
            
        }
    }
}
